[![pipeline status](https://gitlab.com/combate-corona-virus/web/badges/develop/pipeline.svg)](https://gitlab.com/combate-corona-virus/web/commits/develop)

# COVID WEB

## Instalação

```bash
git clone git@gitlab.com:combate-corona-virus/web.git /var/www/web-covid
cd /var/www/web-covid
docker-compose build
docker-compose up
```