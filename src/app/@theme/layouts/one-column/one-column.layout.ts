import { Component } from '@angular/core';
import { Checkbox } from 'app/classes/Checkbox';
import { MapService } from 'app/providers/services/map.service';

@Component({
  selector: 'ngx-one-column-layout',
  styleUrls: ['./one-column.layout.scss'],
  template: `
    <nb-layout windowMode>
      <nb-layout-header fixed>
        <ngx-header></ngx-header>
      </nb-layout-header>

      <nb-sidebar class="menu-sidebar" tag="menu-sidebar" responsive>
        <ng-content select="nb-menu"></ng-content>
      </nb-sidebar>

      <nb-layout-column>
        <ng-content select="router-outlet"></ng-content>
      </nb-layout-column>

      <nb-sidebar class="settings-sidebar menu-lateral"
        tag="settings-sidebar"
        state="collapsed"
        fixed
        end="true">

        <h6>Camadas</h6>
        <checkbox-group (onClick)="onCamadasChange($event)"
        [content]="CheckboxContents.DadosMonitorados" [expanded]=false>
        </checkbox-group>
      </nb-sidebar>

    </nb-layout>
  `,
})
export class OneColumnLayoutComponent {

  CheckboxContents = Checkbox.Contents;

  constructor(private mapService: MapService) {}

  public onCamadasChange($event): void {
    this.mapService.onEmitirEvento(
      Checkbox.Utils.getIDs($event.items.checked),
    );
  }
}
