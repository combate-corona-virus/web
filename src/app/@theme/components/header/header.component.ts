import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  NbMediaBreakpointsService,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
  NbIconLibraries,
} from '@nebular/theme';

import { UserData } from '../../../@core/data/users';
import { LayoutService } from '../../../@core/utils';
import { map, takeUntil } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from 'app/providers/services/auth.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;

  themesNames = {
    'default': 'Light',
    'dark': 'Dark',
  };

  currentTheme;

  userMenu = [ { title: 'Perfil' }, { title: 'Sair' } ];
  private menu: Subscription;

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private themeService: NbThemeService,
              private userService: UserData,
              private layoutService: LayoutService,
              private breakpointService: NbMediaBreakpointsService,
              private router: Router,
              private authService: AuthService,
              iconsLibrary: NbIconLibraries) {
    iconsLibrary.registerFontPack('fa', { packClass: 'fa', iconClassPrefix: 'fa' });
  }

  ngOnInit() {
    if (localStorage.getItem('info_currentTheme') == null) {
      localStorage.setItem('info_currentTheme', 'default');
    }

    this.currentTheme = localStorage.getItem('info_currentTheme');

    this.user = JSON.parse(localStorage.getItem('auth_info'));

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);

    this.menu = this.menuService.onItemClick().subscribe((event) => {
      switch (event.item.title) {
        case 'Sair':
          this.authService.logout();
          break;
        case 'Perfil':
          this.router.navigate(['pages/perfil']);
          break;
      }
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.menu.unsubscribe();
  }

  changeTheme() {
    this.currentTheme = (localStorage.getItem('info_currentTheme') === 'default') ? 'dark' : 'default';
    this.themeService.changeTheme(this.currentTheme);
    localStorage.setItem('info_currentTheme', this.currentTheme);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }
}
