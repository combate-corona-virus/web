import { Observable } from 'rxjs/internal/Observable';
import { pluck } from 'rxjs/internal/operators/pluck';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root',
})
export class MapaCalorGQL {

  constructor(private apollo: Apollo) { }

  getFeatureCollectionQuery = gql`
    query getFeatureCollection($id_geocamada: [Int], $id_municipio: Int, $bounds: Bounds) {
      getFeatureCollection(id_geocamada: $id_geocamada, id_municipio: $id_municipio, bounds: $bounds){
        type
        id_geocamada
        features {
          type
          geometry
          properties
        }
      }
    }
  `;

  getMunicipioQuery = gql`
    query getMunicipio($id: ID!) {
      getMunicipio(id: $id) {
        geom_centro
      }
    }
  `;

  public getFeatureCollection(id_geocamada: number[], id_municipio: number, bounds: any): Observable<any> {
    return this.apollo.query<any>({
      query: this.getFeatureCollectionQuery,
      variables: {
        id_geocamada,
        id_municipio,
        bounds,
      },
    }).pipe(pluck('data', 'getFeatureCollection'));
  }

  public getMunicipio(id: number): Observable<any> {
    return this.apollo.query<any>({
      query: this.getMunicipioQuery,
      variables: {
        id,
      },
    }).pipe(pluck('data', 'getMunicipio'));
  }
}

