import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapaCalorComponent } from './mapa-calor.component';

const routes: Routes = [{
  path: '',
  component: MapaCalorComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapaCalorRoutingModule { }

export const routedComponents = [
  MapaCalorComponent,
];
