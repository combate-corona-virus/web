import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import { fromLonLat } from 'ol/proj';
import BingMaps from 'ol/source/BingMaps';
import 'ol/ol.css';
import { Tile as TileLayer, Heatmap } from 'ol/layer';
import GeoJSON from 'ol/format/GeoJSON';
import OlVectorSource from 'ol/source/Vector';
import { Subscription } from 'rxjs';
import { MapaCalorGQL } from './mapa-calor.GQL';
import { ActivatedRoute } from '@angular/router';
import * as proj from 'ol/proj';
import { XYZ } from 'ol/source';

@Component({
  selector: 'covid-mapa-calor',
  template: `
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <div id="mapa-calor" class="mapa-calor"></div>
  `,
  styleUrls: ['./mapa-calor.component.scss'],
})
export class MapaCalorComponent implements OnInit, AfterViewInit, OnDestroy {

  idMunicipio = 3991;

  public map: Map;
  public inscricao: Subscription;
  public idMunicipiosBuscadosHistorico = [];

  constructor(
    private route: ActivatedRoute,
    private mapaCalorGQL: MapaCalorGQL,
  ) { }

  ngOnInit() {
    this.inscricao = this.route.params.subscribe(
      (params: any) => {
        this.mapaCalorGQL.getMunicipio(params.id).subscribe(
          response =>  {
          this.map.getView().setCenter(fromLonLat(response.geom_centro.coordinates));
          const currentExtent = this.getCurrentExtentLatLon();
            const bounds = {
              x_min: currentExtent[0],
              y_min: currentExtent[1],
              x_max: currentExtent[2],
              y_max: currentExtent[3],
            };

            if (this.idMunicipiosBuscadosHistorico.indexOf(params.id) === -1) {
              this.idMunicipiosBuscadosHistorico.push(params.id);
              this.mapaCalorGQL.getFeatureCollection([15], params.id, bounds).subscribe(
                data => {
                  const fc = [];
                  data.forEach(featureCollection => {
                    if (featureCollection.features.length > 0) {
                      const obj: any = {};
                      obj.fc = featureCollection;
                      fc.push(obj);
                    }
                  });
                  if (fc.length > 0) {
                    this.addGeoJSON(fc);
                  }
                },
              );
            }
          });
      },
    );
  }

  ngAfterViewInit() {
    const raster = new TileLayer({
      visible: true,
      preload: Infinity,
      // source: new BingMaps({
      //   key:
      //     'AoGwOff1xHlD_EsIyFAVwedXJpeC5RObYdYWjtC8xULKeMqhyRT3x4dXZJQg-n2n',
      //   imagerySet: 'Road',
      //   maxZoom: 19,
      // }),
      source: new XYZ({
        url: 'http://mt{0-3}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
      }),
    });

    this.map = new Map({
      layers: [raster],
      target: 'mapa-calor',
      view: new View({
        center: fromLonLat([-49.126135, -22.253992]),
        zoom: 12,
        maxZoom: 14,
        minZoom: 5,
      }),
      // controls: [],
    });
  }

  addGeoJSON(data: any[]): boolean {

    if (!data) {
      return false;
    }
    const len = data.length;
    for (let i = 0; i < len; i++) {

      const features = (new GeoJSON()).readFeatures(data[i].fc, {
        featureProjection: 'EPSG:3857',
      });

        const heatMapLayer = new Heatmap({
          source: new OlVectorSource({
            features,
          }),
          radius: 4,
          blur: 20,
        });
        heatMapLayer.setProperties({ id_geocamada: data[i].fc.id_geocamada });
        this.map.addLayer(heatMapLayer);
    }
    return true;
  }

   /**
   * Calcula extent da view atual do mapa. SRID 3857
   *
   * @return {Array<number>} [minX, minY, maxX, maxY]
   * minX, minY - Canto inferior esquerdo
   * maxX, maxY - Canto superior direito
   */
  public getCurrentExtent() {
    return this.map.getView().calculateExtent(this.map.getSize());
  }


  /**
   * Calcula extent da view atual do mapa. SRID 4326
   *
   * @return {Array<number>} [minX, minY, maxX, maxY]
   */
  public getCurrentExtentLatLon(): number[] {
    const extent = this.getCurrentExtent();
    return proj.transformExtent(extent, 'EPSG:3857', 'EPSG:4326');
  }

  ngOnDestroy() {
    this.inscricao.unsubscribe();
  }
}
