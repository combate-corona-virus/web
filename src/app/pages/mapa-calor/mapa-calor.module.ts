import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { MapaCalorRoutingModule, routedComponents } from './mapa-calor-routing.module';
import { MapaCalorGQL } from './mapa-calor.GQL';

@NgModule({
  imports: [
    ThemeModule,
    MapaCalorRoutingModule,
  ],
  exports: [
    ...routedComponents,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    MapaCalorGQL,
  ],
})
export class MapaCalorModule { }
