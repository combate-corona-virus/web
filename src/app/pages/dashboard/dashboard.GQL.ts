import { Observable } from 'rxjs/internal/Observable';
import { pluck } from 'rxjs/internal/operators/pluck';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root',
})
export class DashbordGQL {

  constructor(private apollo: Apollo) { }

  getGraficoCondicaoPacientePizzaQuery = gql`
    query getGraficoCondicaoPacientePizza($id_municipio: Int!) {
      getGraficoCondicaoPacientePizza(id_municipio: $id_municipio) {
        name
        value
      }
    }
  `;

  getGraficoCondicaoPacienteLinhaQuery = gql`
    query getGraficoCondicaoPacientePizza($id_municipio: Int!) {
      getGraficoCondicaoPacienteLinha(id_municipio: $id_municipio) {
        name
        series {
          name
          value
        }
      }
    }
  `;

  getGraficoPatogenoQuery = gql`
    query getGraficoPatogeno($id_municipio: Int!) {
      getGraficoPatogeno(id_municipio: $id_municipio) {
        name
        series {
          name
          value
        }
      }
    }
  `;

  getGraficoFaixaEtariaQuery = gql`
    query getGraficoFaixaEtaria($id_municipio: Int!) {
      getGraficoFaixaEtaria(id_municipio: $id_municipio) {
        name
        value
      }
    }
  `;

  getGraficoNorificacoesPorDiaQuery = gql`
    query getGraficoNorificacoesPorDia($id_municipio: Int!) {
      getGraficoNorificacoesPorDia(id_municipio: $id_municipio) {
        name
        series {
          name
          value
        }
      }
    }
  `;

  getGraficoTotalizadoresPercentuaisQuery = gql`
    query getGraficoTotalizadoresPercentuais($id_municipio: Int!) {
      getGraficoTotalizadoresPercentuais(id_municipio: $id_municipio) {
        name
        formated_name
        value
      }
    }
  `;

  getGraficoTotalizadoresQuery = gql`
    query getGraficoTotalizadores($id_municipio: Int!) {
      getGraficoTotalizadores(id_municipio: $id_municipio) {
        name
        value
      }
    }
  `;

  getDataUltimaImportacaoQuery = gql`
    query getPaginatedPaciente($id_municipio: [Int]) {
      getPaginatedPaciente(
        first:1
        filter: {
          id_municipio: $id_municipio
          orderBy:{
            field: "created_at"
            order: DESC
          }})
        {
          data {
            created_at
          }
        }
    }
  `;

  getRegiaoAdministrativaQuery = gql`
    query getRegiaoAdministrativa($filter: FindFilterInput!) {
      getRegiaoAdministrativa(filter: $filter) {
        id
        nome
        municipio {
          id
          nome
          ultimoBoletimEpidemiologico {
            id
            created_at
          }
        }
      }
    }
  `;

  getCalculoMunicipioQuery = gql`
    query getCalculoMunicipio($id_municipio: Int!) {
      getCalculoMunicipio(id_municipio: $id_municipio) {
        name
        value
      }
    }
  `;

  getCalculoRegionalQuery = gql`
    query getCalculoRegional {
      getCalculoRegional {
        name
        value
      }
    }
  `;

  getCalculoIprRegionalQuery = gql`
    query getCalculoIprRegional {
      getCalculoIprRegional {
        name
        value
      }
    }
  `;

  public getGraficoCondicaoPacientePizza(id_municipio: number): Observable<any> {
    return this.apollo.watchQuery<any>({
      query: this.getGraficoCondicaoPacientePizzaQuery,
      variables: {
        id_municipio,
      },
    }).valueChanges.pipe(pluck('data', 'getGraficoCondicaoPacientePizza'));
  }

  public getGraficoCondicaoPacienteLinha(id_municipio: number): Observable<any> {
    return this.apollo.watchQuery<any>({
      query: this.getGraficoCondicaoPacienteLinhaQuery,
      variables: {
        id_municipio,
      },
    }).valueChanges.pipe(pluck('data', 'getGraficoCondicaoPacienteLinha'));
  }

  public getGraficoPatogeno(id_municipio: number): Observable<any> {
    return this.apollo.watchQuery<any>({
      query: this.getGraficoPatogenoQuery,
      variables: {
        id_municipio,
      },
    }).valueChanges.pipe(pluck('data', 'getGraficoPatogeno'));
  }

  public getGraficoFaixaEtaria(id_municipio: number): Observable<any> {
    return this.apollo.watchQuery<any>({
      query: this.getGraficoFaixaEtariaQuery,
      variables: {
        id_municipio,
      },
    }).valueChanges.pipe(pluck('data', 'getGraficoFaixaEtaria'));
  }

  public getGraficoNorificacoesPorDia(id_municipio: number): Observable<any> {
    return this.apollo.watchQuery<any>({
      query: this.getGraficoNorificacoesPorDiaQuery,
      variables: {
        id_municipio,
      },
    }).valueChanges.pipe(pluck('data', 'getGraficoNorificacoesPorDia'));
  }

  public getGraficoTotalizadoresPercentuais(id_municipio: number): Observable<any> {
    return this.apollo.watchQuery<any>({
      query: this.getGraficoTotalizadoresPercentuaisQuery,
      variables: {
        id_municipio,
      },
    }).valueChanges.pipe(pluck('data', 'getGraficoTotalizadoresPercentuais'));
  }

  public getGraficoTotalizadores(id_municipio: number): Observable<any> {
    return this.apollo.watchQuery<any>({
      query: this.getGraficoTotalizadoresQuery,
      variables: {
        id_municipio,
      },
    }).valueChanges.pipe(pluck('data', 'getGraficoTotalizadores'));
  }

  public getDataUltimaImportacao(id_municipio: number[]): Observable<any> {
    return this.apollo.query<any>({
      query: this.getDataUltimaImportacaoQuery,
      variables: {
        id_municipio,
      },
    }).pipe(pluck('data', 'getPaginatedPaciente', 'data'));
  }

  public getRegiaoAdministrativa(filter: any): Observable<any> {
    return this.apollo.query<any>({
      query: this.getRegiaoAdministrativaQuery,
      variables: {
        filter,
      },
    }).pipe(pluck('data', 'getRegiaoAdministrativa'));
  }

  public getCalculoMunicipio(id_municipio: number): Observable<any> {
    return this.apollo.query<any>({
      query: this.getCalculoMunicipioQuery,
      variables: {
        id_municipio,
      },
    }).pipe(pluck('data', 'getCalculoMunicipio'));
  }

  public getCalculoRegional(): Observable<any> {
    return this.apollo.query<any>({
      query: this.getCalculoRegionalQuery,
      variables: {},
    }).pipe(pluck('data', 'getCalculoRegional'));
  }

  public getCalculoIprRegional(): Observable<any> {
    return this.apollo.query<any>({
      query: this.getCalculoIprRegionalQuery,
      variables: {},
    }).pipe(pluck('data', 'getCalculoIprRegional'));
  }
}

