import { Component, OnInit } from '@angular/core';
import { DashbordGQL } from './dashboard.GQL';
import { AuthService } from 'app/providers/services/auth.service';
import { TimeagoIntl } from 'ngx-timeago';
import {strings as ptBrStrings} from 'ngx-timeago/language-strings/pt';
import * as moment from 'moment';
import { LoggedUserService } from 'app/providers/services/logged-user.service';


@Component({
  selector: 'covid-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
  id_municipio = AuthService.getToken().id_municipio;
  condicaoPacienteTotal = [];
  evolucaoCondicaoPaciente = [];
  evolucaoCasos = [];
  obitosFaixaEtaria = [];
  evolucaoNotificacoes = [];
  totalizadoresPercentuais = [];
  totalizadoresAbsolutos = [];
  totalizadoresIndicadores = [];
  data_atualizada = '';
  moment = moment;
  timestampDataAtualizada: number = -1;

  boletins: any = [];

  calculoMunicipal = [];
  calculoRegional = [];
  calculoIprRegional = [];

  symbolFormatting = (data => {
    if (data.label === '<b>Letalidade</b> <br>(óbitos pelo total)')
      return data.value + '%';
    return data.value + '';
  });

  totalizadoresAbsolutosScheme = { domain: ['#C70039', '#239B56', '#FFC300', '#5cafe2', '#8c8c8c', '#000000'] };
  totalizadoresPercentuaisScheme = { domain: ['#E6B0AA', '#FAD7A0', '#ABB2B9']};
  condicoesPacienteScheme = { domain: ['#239B56', '#FFC300', '#E67E22', '#C70039', '#000000']};

  constructor(
    intl: TimeagoIntl,
    private dashbordGQL: DashbordGQL,
    private loggedUserService: LoggedUserService,
  ) {
    intl.strings = ptBrStrings;
    intl.changes.next();
  }

  ngOnInit() {
    this.dashbordGQL
      .getDataUltimaImportacao([this.id_municipio])
      .subscribe(response => {
        if (response.length > 0) {
          this.data_atualizada = response[0].created_at;
          this.timestampDataAtualizada = new Date(response[0].created_at).getTime();
        }
      });

    this.dashbordGQL
      .getGraficoCondicaoPacientePizza(this.id_municipio)
      .subscribe(data => (this.condicaoPacienteTotal = data));

    this.dashbordGQL
      .getGraficoCondicaoPacienteLinha(this.id_municipio)
      .subscribe(data => (this.evolucaoCondicaoPaciente = data));

    this.dashbordGQL
      .getGraficoPatogeno(this.id_municipio)
      .subscribe(data => (this.evolucaoCasos = data));

    this.dashbordGQL
      .getGraficoFaixaEtaria(this.id_municipio)
      .subscribe(data => (this.obitosFaixaEtaria = data));

    this.dashbordGQL
      .getGraficoNorificacoesPorDia(this.id_municipio)
      .subscribe(data => (this.evolucaoNotificacoes = data));

    this.dashbordGQL
      .getGraficoTotalizadoresPercentuais(this.id_municipio)
      .subscribe(data => (
        this.totalizadoresPercentuais = data.map(function (totalizador) {
          return {
            name: totalizador.formated_name,
            value: totalizador.value,
            extra: totalizador.extra,
          };
        })
      ));

    this.dashbordGQL
      .getGraficoTotalizadores(this.id_municipio)
      .subscribe(data => (this.totalizadoresAbsolutos = data));

    this.loggedUserService.userInfoAsync.subscribe(userInfo => {
      this.dashbordGQL.getRegiaoAdministrativa({id: 1}).subscribe(
        (data: any) => {
          const { id } = userInfo.municipio;
          if ('undefined' === typeof data.municipio.find(mun => mun.id === id)) {
            data.municipio.push(userInfo.municipio);
          }
          const boletins = [];
          data.municipio.forEach((element) => {
            const boletim: any = {};
            boletim.nome = element.nome;
            if (element.ultimoBoletimEpidemiologico != null) {
              boletim.created_at = element.ultimoBoletimEpidemiologico.created_at;
              boletim.date = new Date(element.ultimoBoletimEpidemiologico.created_at);
              boletim.timestamp = new Date(element.ultimoBoletimEpidemiologico.created_at).getTime();
            } else {
              boletim.date = new Date().setDate(new Date().getDate() - 2);
              boletim.timestamp = -1;
            }
            boletins.push(boletim);
          });
          this.boletins = boletins.sort((a, b) => b.date - a.date);
        },
      );
    });

    this.dashbordGQL.getCalculoMunicipio(this.id_municipio).subscribe(
      (data: any[]) => {
        if (data.length > 0) {
          this.calculoMunicipal[0] = data.slice(0, 5);
          this.calculoMunicipal[1] = data.slice(5, 10);
        }
      },
    );

    this.dashbordGQL.getCalculoRegional().subscribe(
      data => {
        this.calculoRegional = data;

        if (data[2].value === 'Dados preenchidos') {
          this.dashbordGQL.getCalculoIprRegional().subscribe(
            (response: any[]) => {
              if (response.length > 0) {
                this.calculoIprRegional[0] = response.slice(0, 5);
                this.calculoIprRegional[1] = response.slice(5, 10);
              }
            },
          );
        }
      },
    );
  }
}
