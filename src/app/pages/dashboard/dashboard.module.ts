import { NgModule, LOCALE_ID } from '@angular/core';
import { PercentPipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbTabsetModule,
  NbUserModule,
  NbRadioModule,
  NbSelectModule,
  NbListModule,
  NbIconModule,
} from '@nebular/theme';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ThemeModule } from '../../@theme/theme.module';

import { DashboardRoutingModule, routedComponents } from './dashboard-routing.module';

import localePtBr from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { TimeagoModule, TimeagoIntl, TimeagoFormatter, TimeagoCustomFormatter } from 'ngx-timeago';
registerLocaleData(localePtBr);

export class MyIntl extends TimeagoIntl {}

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbTabsetModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbListModule,
    NbIconModule,
    NbButtonModule,
    NgxChartsModule,
    DashboardRoutingModule,
    TimeagoModule.forChild({
      intl: { provide: TimeagoIntl, useClass: MyIntl },
      formatter: { provide: TimeagoFormatter, useClass: TimeagoCustomFormatter }
    }),
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    PercentPipe,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
  ],
  exports: [
    TimeagoModule,
  ],
})
export class DashboardModule { }
