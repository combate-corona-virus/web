import { NgModule } from '@angular/core';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule } from '@nebular/theme';

import { DenunciasRoutingModule, routedComponents } from './denuncias-routing.module';

@NgModule({
  imports: [
    DenunciasRoutingModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbTreeGridModule,
    NbButtonModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class DenunciasModule { }
