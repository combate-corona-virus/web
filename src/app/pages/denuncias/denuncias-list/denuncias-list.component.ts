import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'covid-denuncias-list',
  templateUrl: './denuncias-list.component.html',
  styleUrls: ['./denuncias-list.component.scss'],
})
export class DenunciasListComponent implements OnInit {

  denuncias: any;

  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.denuncias = [
      {
        'id': 1,
        'titulo': 'Denuncia 1',
        'endereco': 'Rua teste',
        'data_criacao': '27/03/2020',
      },
    ];
  }

  goToCreateDenuncia() {
    this.router.navigate(['pages/denuncias/add']);
  }

  goToEditDenuncia(id: number) {
    this.router.navigate([`pages/denuncias/${id}/edit`]);
  }

}
