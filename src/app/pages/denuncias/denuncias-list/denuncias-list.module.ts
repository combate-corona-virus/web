import { NgModule } from '@angular/core';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule } from '@nebular/theme';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';

import { DenunciasListRoutingModule, routedComponents } from './denuncias-list-routing.module';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    DenunciasListRoutingModule,
    TableModule,
    ButtonModule,
    NbButtonModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class DenunciasListModule { }
