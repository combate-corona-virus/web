import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DenunciasComponent } from './denuncias.component';

const routes: Routes = [
  {
    path: '',
    component: DenunciasComponent,
    children: [
      {
        path: 'list',
        loadChildren: () => import('./denuncias-list/denuncias-list.module')
        .then(m => m.DenunciasListModule),
      },
      {
        path: 'add',
        loadChildren: () => import('./denuncias-form/denuncias-form.module')
        .then(m => m.DenunciasFormModule),
      },
      {
        path: ':id',
        children: [
          {
            path: 'edit',
            loadChildren: () => import('./denuncias-form/denuncias-form.module')
            .then(m => m.DenunciasFormModule),
          },
          {
            path: '**',
            redirectTo: 'edit',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: '**',
        redirectTo: 'list',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DenunciasRoutingModule { }

export const routedComponents = [
  DenunciasComponent,
];
