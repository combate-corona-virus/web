import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'covid-denuncias-form',
  templateUrl: './denuncias-form.component.html',
  styleUrls: [ './denuncias-form.component.scss'],
})
export class DenunciasFormComponent  implements OnInit {

    form: FormGroup;

    constructor(private formBuilder: FormBuilder, private location: Location) { }

    ngOnInit() {
        this.form = this.formBuilder.group({
            'titulo': [null, Validators.required],
            'descricao': [null, Validators.required],
            'endereco': [null, Validators.required],
        }, {});
    }

    submit(formValue: any) {}

    cancel() {
        this.location.back();
      }
}
