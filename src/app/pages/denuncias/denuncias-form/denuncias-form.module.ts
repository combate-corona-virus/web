import { NgModule } from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from '@nebular/theme';
import { FormsModule as ngFormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThemeModule } from '../../../@theme/theme.module';
import { CommonModule } from '@angular/common';
import { DenunciasFormRoutingModule } from './denuncias-form-routing.module';
import {FileUploadModule} from 'primeng/fileupload';
import { DenunciasFormComponent } from './denuncias-form.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DenunciasFormRoutingModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    FileUploadModule,
  ],
  declarations: [
    DenunciasFormComponent,
  ],
})
export class DenunciasFormModule { }
