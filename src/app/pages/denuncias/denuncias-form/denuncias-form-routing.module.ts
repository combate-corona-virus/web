import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DenunciasFormComponent } from './denuncias-form.component';

const routes: Routes = [
  {
  path: '',
  component: DenunciasFormComponent,
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DenunciasFormRoutingModule { }

export const routedComponents = [
  DenunciasFormComponent,
];
