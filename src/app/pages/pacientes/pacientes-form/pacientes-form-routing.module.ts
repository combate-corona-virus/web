import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PacientesFormComponent } from './pacientes-form.component';

const routes: Routes = [
  {
  path: '',
  component: PacientesFormComponent,
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PacientesFormRoutingModule { }

export const routedComponents = [
    PacientesFormComponent,
];
