import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PacientesViewComponent } from './pacientes-view.component';

const routes: Routes = [
  {
  path: '',
  component: PacientesViewComponent,
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PacientesViewRoutingModule { }

export const routedComponents = [
    PacientesViewComponent,
];
