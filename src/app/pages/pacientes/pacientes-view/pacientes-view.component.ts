import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterHelper } from 'app/providers/utils/router-helper.util';

@Component({
  selector: 'covid-pacientes-view',
  templateUrl: './pacientes-view.component.html',
  styleUrls: ['./pacientes-view.component.scss'],
})
export class PacientesViewComponent implements OnInit {

  resolvedPaciente: any;

  constructor(
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    RouterHelper.getResolvedData(this.route, 'paciente').subscribe(resolvedData => {
      this.resolvedPaciente = resolvedData;
    });
  }
}
