import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule } from '@nebular/theme';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';

import { PacienteResolver } from 'app/providers/services/paciente-resolver.service';
import { PacientesViewRoutingModule, routedComponents } from './pacientes-view-routing.module';

@NgModule({
  imports: [
    CommonModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    PacientesViewRoutingModule,
    TableModule,
    ButtonModule,
    NbButtonModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    PacienteResolver,
  ],
})
export class PacientesViewModule { }
