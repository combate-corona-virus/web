import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PacienteResolver } from 'app/providers/services/paciente-resolver.service';
import { PacientesComponent } from './pacientes.component';


const routes: Routes = [
  {
    path: '',
    component: PacientesComponent,
    children: [
      {
        path: 'list',
        loadChildren: () => import('./pacientes-list/pacientes-list.module')
          .then(m => m.PacientesListModule),
      },
      {
        path: 'add',
        loadChildren: () => import('./pacientes-form/pacientes-form.module')
          .then(m => m.PacientesFormModule),
      },
      {
        path: ':id',
        resolve: { paciente: PacienteResolver },
        children: [
          {
            path: 'view',
            loadChildren: () => import('./pacientes-view/pacientes-view.module')
              .then(m => m.PacientesViewModule),
          },
          {
            path: 'edit',
            loadChildren: () => import('./pacientes-form/pacientes-form.module')
              .then(m => m.PacientesFormModule),
          },
          {
            path: '**',
            redirectTo: 'view',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: '**',
        redirectTo: 'list',
        pathMatch: 'full',
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PacientesRoutingModule { }

export const routedComponents = [
  PacientesComponent,
];
