import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { NbWindowService } from '@nebular/theme';

import { TabelaBase } from 'app/classes/TabelaBase';
import { ImportacaoService } from 'app/providers/services/importacao.service';
import { PacienteGQL } from '../paciente.GQL';
import { AuthService } from 'app/providers/services/auth.service';
import { ValidationUtil } from 'app/providers/utils/validation.util';

@Component({
  selector: 'covid-pacientes-list',
  templateUrl: './pacientes-list.component.html',
  styleUrls: ['./pacientes-list.component.scss'],
})
export class PacientesListComponent extends TabelaBase implements OnInit {

  pacientes: any;
  id_municipio: number = AuthService.getToken().id_municipio;

  sexoOptions: any = [
    { key: 'F', value: 'Feminino' },
    { key: 'M', value: 'Masculino' },
  ];

  @ViewChild('disabledEsc', { read: TemplateRef, static: true }) disabledEscTemplate: TemplateRef<HTMLElement>;

  constructor(
    router: Router,
    route: ActivatedRoute,
    private fb: FormBuilder,
    private pacienteGLQ: PacienteGQL,
    private importacaoService: ImportacaoService,
    private windowService: NbWindowService,
  ) {
    super(router, route);
  }

  ngOnInit() {
    this.form = this.fb.group({
      numero_notificacao: [null, ValidationUtil.isNumber],
      data_notificacao_inicio: null,
      data_notificacao_final: null,
      faixa_etaria: null,
      sexo: null,
      tipo: null,
      condicao: null,
    });
    this.listObservableCallback = params => this.pacienteGLQ.search({ ...params }, this.id_municipio);
    super.initializeComponent();
  }

  onEditPaciente(id) {
    this.router.navigate([`pages/pacientes/edit/${id}`]);
  }

  goToDownload() {
    this.importacaoService.downloadPlanilha().subscribe();
  }

  openWindowWithoutBackdrop() {
    this.windowService.open(this.disabledEscTemplate, {
      title: 'INSTRUÇÕES DE PREENCHIMENTO',
      hasBackdrop: true,
      closeOnEsc: true,
      closeOnBackdropClick: false,
      windowClass: 'window-limited-height',
    });
  }

  downloadPlanilhaExemplo() {
    const link = document.createElement('a');
    link.setAttribute('type', 'hidden');
    link.href = 'assets/files/Exemplo - Combate Coronavírus - Planilha de controle de pacientes.xlsx';
    document.body.appendChild(link);
    link.click();
    link.remove();
  }
}
