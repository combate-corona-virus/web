import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule } from '@nebular/theme';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { FileUploadModule } from 'primeng/fileupload';

import { FilterPipe } from 'app/pipes/filter.pipe';
import { ImportacaoService } from 'app/providers/services/importacao.service';
import { PacientesListRoutingModule, routedComponents } from './pacientes-list-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SomaCalendarModule } from 'app/components/soma-calendar/soma-calendar.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    PacientesListRoutingModule,
    TableModule,
    ButtonModule,
    NbButtonModule,
    FileUploadModule,
    SomaCalendarModule,
  ],
  declarations: [
    ...routedComponents,
    FilterPipe,
  ],
  providers: [
    ImportacaoService,
  ],
})
export class PacientesListModule { }
