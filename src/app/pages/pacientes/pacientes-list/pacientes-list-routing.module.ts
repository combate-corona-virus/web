import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PacientesImportacaoComponent } from './pacientes-importacao/pacientes-importacao.component';
import { PacientesListComponent } from './pacientes-list.component';

const routes: Routes = [
  {
    path: '',
    component: PacientesListComponent,
  },
  {
    path: 'import',
    component: PacientesImportacaoComponent,
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PacientesListRoutingModule { }

export const routedComponents = [
    PacientesListComponent,
    PacientesImportacaoComponent,
];
