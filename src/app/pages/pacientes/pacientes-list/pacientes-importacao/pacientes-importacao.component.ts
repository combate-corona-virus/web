import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ImportacaoService } from 'app/providers/services/importacao.service';
import Swal from 'sweetalert2';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'covid-pacientes-importacao',
  templateUrl: './pacientes-importacao.component.html',
  styleUrls: ['./pacientes-importacao.component.scss'],
})
export class PacientesImportacaoComponent implements OnInit {
  uploadedFile: File = null;

  constructor(
    private location: Location,
    private importacaoService: ImportacaoService,
  ) {}

  ngOnInit() {}

  onSubmit() {
    Swal.fire({
      title: 'Importação em andamento...',
      text: 'Aguarde um momento.',
    });
    Swal.showLoading();
    const file = this.uploadedFile;
    this.uploadedFile = null;
    this.importacaoService.postWithFile(file).subscribe(
      () => {
        Swal.close();
        Swal.fire(
          'Planilha importada!',
          'Importação da planilha realizada com sucesso.',
          'success',
        );
      },
      () => {
        Swal.close();
        Swal.fire(
          'Erro na importação!',
          `A planilha contém erros de preenchimento. Tente novamente após corrigi-los.`,
          'error',
        );
      },
    );
  }

  public onSelectImage(evt: any) {
    this.uploadedFile = evt[0];
  }

  cancel() {
    this.location.back();
  }
}
