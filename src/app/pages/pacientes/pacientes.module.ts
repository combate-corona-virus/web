import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbTreeGridModule,
  NbButtonModule,
  NbActionsModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbUserModule,
  NbSelectModule,
  NbRadioModule,
} from '@nebular/theme';


import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { FileUploadModule } from 'primeng/fileupload';

import { PacienteResolver } from 'app/providers/services/paciente-resolver.service';
import { PacientesRoutingModule, routedComponents } from './pacientes-routing.module';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    PacientesRoutingModule,
    TableModule,
    ButtonModule,
    NbButtonModule,
    FormsModule,
    ReactiveFormsModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule,
    FileUploadModule,
    CommonModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    PacienteResolver,
  ],
})
export class PacientesModule { }
