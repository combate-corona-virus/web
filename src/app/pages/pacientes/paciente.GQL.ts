import { Observable } from 'rxjs/internal/Observable';
import { pluck } from 'rxjs/internal/operators/pluck';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import * as moment from 'moment';


@Injectable()
export class PacienteGQL {

  constructor(private apollo: Apollo) { }

  getPaginatedPacienteQuery = gql`
    query getPaginatedPaciente($first: Int!, $page: Int, $filter: PacienteFilterInput) {
      getPaginatedPaciente(first: $first, page: $page, filter: $filter) {
        paginatorInfo {
          total
        }
        data {
          id
          geom
          numero_notificacao
          data_notificacao
          faixa_etaria
          sexo
          endereco
          condicao
          tipo
        }
      }
    }
  `;

  getPacienteQuery = gql`
    query getPaciente($id: ID!) {
      getPaciente(id: $id) {
        id
        tipo
        numero_notificacao
        endereco
        residencia
        data_notificacao
        sexo
        idade
        faixa_etaria
        condicao
        data_alta_obito
        status_exame
        data_coleta
        agente_patogeno
        created_at
        updated_at
      }
    }
  `;

  public search(filter, id_municipio): Observable<any> {

    const {
      limit,
      offset,
      data_notificacao_inicio,
      data_notificacao_final,
      sortField,
      sortOrder,
    } = filter;

    filter['orderBy'] = [
      {
        field: (sortField != null ? sortField : 'data_notificacao'),
        order: (sortOrder === 1 ? 'ASC' : 'DESC'),
      },
    ];

    filter['id_municipio'] = id_municipio;

    if (data_notificacao_inicio != null) {
      filter['data_notificacao'] = {
        from: moment(data_notificacao_inicio).format('YYYY-MM-DD'),
        to: moment(data_notificacao_final).format('YYYY-MM-DD'),
      };
    }

    [
      'limit',
      'offset',
      'sortField',
      'sortOrder',
      'data_notificacao_inicio',
      'data_notificacao_final',
    ].forEach(item => delete filter[item]);

    return this.apollo.query<any>({
      query: this.getPaginatedPacienteQuery,
      variables: {
        first: limit,
        page: Math.floor((offset / limit) + 1),
        filter,
      },
    }).pipe(pluck('data', 'getPaginatedPaciente'));
  }

  public get(idPaciente: any): Observable<any> {
    return this.apollo.query<any>({
      query: this.getPacienteQuery,
      variables: {
        id: idPaciente,
      },
    }).pipe(pluck('data', 'getPaciente'));
  }
}

