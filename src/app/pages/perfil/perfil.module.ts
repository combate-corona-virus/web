import { NgModule } from '@angular/core';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule } from '@nebular/theme';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';

import { PerfilRoutingModule, routedComponents } from './perfil-routing.module';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    PerfilRoutingModule,
    TableModule,
    ButtonModule,
    NbButtonModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class PerfilModule { }
