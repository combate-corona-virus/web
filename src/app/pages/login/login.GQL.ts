import { Observable } from 'rxjs/internal/Observable';
import { pluck } from 'rxjs/internal/operators/pluck';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root',
})
export class LoginGQL {

  constructor(private apollo: Apollo) { }

  loginMutation = gql`
    mutation login($email: String, $password: String) {
      login(email: $email, password: $password, ) {
        nome
        sobrenome
        auth_token
        id_municipio
        municipio {
          nome
          geom_centro
          unidadeFederativa {
            nome
          }
        }
      }
    }
  `;

  public login(email: string, password: string): Observable<any> {
    return this.apollo.mutate<any>({
      mutation: this.loginMutation,
      variables: {
        email,
        password,
      },
    }).pipe(pluck('data', 'login'));
  }
}

