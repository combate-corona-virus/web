import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NbCardModule, NbButtonModule, NbInputModule, NbLayoutModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';

import { PerfilRoutingModule, routedComponents } from './login-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    ThemeModule,
    NbLayoutModule,
    PerfilRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class LoginModule {}
