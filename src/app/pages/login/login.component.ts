import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginGQL } from './login.GQL';
import { AuthService } from 'app/providers/services/auth.service';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { LoggedUserService } from 'app/providers/services/logged-user.service';

@Component({
  selector: 'covid-login',
  styleUrls: ['./login.component.scss'],
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  submitted: boolean = false;
  unauthorized: boolean = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private loginGQL: LoginGQL,
    private authService: AuthService,
    private loggedUserService: LoggedUserService,
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'email':
        [{ value: null, disabled: this.submitted }, Validators.compose([Validators.required, Validators.minLength(4)])],
      'password':
        [{ value: null, disabled: this.submitted }, Validators.compose([Validators.required, Validators.minLength(4)])],
    });
  }

  onSubmit(values: any): void {
    this.unauthorized = false;
    this.submitted = true;
    const { email, password } = values;

    this.loginGQL.login(email, password).pipe(
      tap(response => {
        AuthService.setToken(response);
      }),
      switchMap(() => this.loggedUserService.getLoggedUser()),
    ).subscribe(
      () => {
        this.authService.previousUrl ?
        this.router.navigate(this.authService.previousUrl) :
        this.router.navigate(['/pages/tutorial']);
      },
      () => {
        this.unauthorized = true;
        this.submitted = false;
      },
    );
  }
}
