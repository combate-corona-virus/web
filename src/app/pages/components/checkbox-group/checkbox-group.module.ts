import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckboxGroupComponent } from './checkbox-group.component';
import { CheckboxItemComponent } from './components/checkbox-item/checkbox-item.component';
import { FormsModule } from '@angular/forms';
import { CheckboxGQL } from './checkbox.GQL';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [
    CheckboxGroupComponent,
    CheckboxItemComponent,
  ],
  entryComponents: [
    CheckboxGroupComponent,
    CheckboxItemComponent,
  ],
  exports: [
    CheckboxGroupComponent,
    CheckboxItemComponent,
  ],
  providers: [
    CheckboxGQL,
  ],
})
export class CheckBoxGroupModule { }
