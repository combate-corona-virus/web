import { Component, ViewEncapsulation, Input, AfterViewInit } from '@angular/core';
import { Checkbox } from 'app/classes/Checkbox';
import { Subject } from 'rxjs/internal/Subject';
import * as jQuery from 'jquery';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'checkbox-item',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './checkbox-item.component.html',
  styleUrls: ['./checkbox-item.component.scss'],
})
export class CheckboxItemComponent implements AfterViewInit {
  @Input() checkListener: Subject<any>;
  @Input() extCheckListener: Subject<any>;
  @Input() colorListener?: Subject<any>;
  @Input() optionItem: any;
  @Input() options: any;
  @Input() disabled?: boolean = false;

  ngAfterViewInit() {
    // console.log(this.optionItem);
    this.extCheckListener.subscribe(change => {
      const checkedPrev = change.previous.indexOf(this.optionItem.id) !== -1;
      const checkedCurr = change.current.indexOf(this.optionItem.id) !== -1;

      if (checkedPrev !== checkedCurr) {
        this.optionItem.checked = !checkedCurr;
        this.selectItem(this.optionItem);
      } else if (this.optionItem.checked === !checkedCurr) {
        this.selectItem(this.optionItem);
      }
    });
  }

  trataLabelClick($event, item: Checkbox.Item): boolean {
    if (item.geocamadas_filhas.length > 0) {
      this.toggleSubList($event, item);
    } else {
      this.selectItem(item);
    }

    return false;
  }

  toggleSubList($event, item: Checkbox.Item): boolean {
    const sublist = jQuery('ul[id=' + item.id + ']');

    $event.item = item;
    $event.item.expanded = !$event.item.expanded;

    sublist.slideToggle();

    return false;
  }

  /**
   * Atualiza o item clicado e dispara o evento de seleção
   *
   * @param {Item} item
   * @param {*} e
   */
  selectItem(item: Checkbox.Item): void {
    // tslint:disable-next-line: no-extra-boolean-cast
    item.checked = !!item.checked ? !item.checked : true;
    this.checkListener.next(this.getItems());
  }

  /**
   * Atualiza o item dispara o evento de troca de cor
   *
   * @param {Checkbox.Item} item
   * @param {string} color
   */
  onColorChange(item: Checkbox.Item, color: string): void {
    item.cor = color;
    this.colorListener.next(this.getItems());
  }

  get corIcone() {
    if (this.disabled) {
      return 'rgba(255, 255, 255, 0.4)';
    }
    if (this.optionItem && this.optionItem.cor) {
      return this.optionItem.cor;
    }
    return 'rgba(255, 255, 255, 0.4)';
  }

  /**
   * Retorna o item 'pai'(agrupador) do item
   *
   * @private
   * @param {any} item
   * @returns {Item}
   */
  private getGroupItem(item): Checkbox.Item {
    const items: Checkbox.Item[] = Checkbox.Utils.joinItems(this.options);
    return items.find(group => group.id === item.id_geocamada_pai);
  }

  /**
   * Retorna um objeto com os ids dos itens
   * agrupados em arrays de 'marcado' e 'desmarcado', descartando itens 'pais' (agrupadores).
   *
   * @private
   * @returns {*}
   */
  private getItems(): any {
    const allItems = Checkbox.Utils.joinItems(this.options);
    // Descarta itens agrupadores
    return {
      checked: allItems
        .filter(item => item.checked && item.geocamadas_filhas.length === 0)
        .map(checked => {
          return {
            id: checked.id,
            cor: this.getItemColor(checked),
            icone: checked.icone,
            nome: checked.nome,
          };
        }),
      unchecked: allItems
        .filter(item => !item.checked && item.geocamadas_filhas.length === 0)
        .map(unchecked => {
          return {
            id: unchecked.id,
            nome: unchecked.nome,
          };
        }),
    };
  }

  private getItemColor(item): string {
    // tslint:disable-next-line: no-extra-boolean-cast
    return !!item.cor ? item.cor : this.getItemColor(this.getGroupItem(item));
  }

  getClass(item): string {
    if (item.expanded) {
      return 'item-sublist';
    } else {
      return 'item-sublist-hidden';
    }
  }
}
