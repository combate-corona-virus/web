import {
  Component,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter,
  AfterViewInit,
  OnInit,
  OnChanges,
} from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Checkbox } from 'app/classes/Checkbox';
import { CheckboxGQL } from './checkbox.GQL';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'checkbox-group',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './checkbox-group.component.html',
  styleUrls: ['./checkbox-group.component.scss'],
})
export class CheckboxGroupComponent implements AfterViewInit, OnInit, OnChanges {
  options: any[] = [];
  colorListener: Subject<any>;
  checkListener: Subject<any> = new Subject();
  extCheckListener: Subject<any> = new Subject();

  @Input() content?: Checkbox.Content;
  @Input() checkedIds?: number[];
  @Input() disabled?: boolean = false;
  @Input() contagem?: boolean = false;
  @Input() expanded?: boolean = false;
  @Output() onClick: EventEmitter<any> = new EventEmitter();
  @Output() onColorChange?: EventEmitter<any> = new EventEmitter();
  @Output() onLoad?: EventEmitter<any> = new EventEmitter();

  constructor(
    private checkboxGQL: CheckboxGQL,
  ) { }

  ngOnChanges(changes) {
    if (changes.checkedIds) {
      const checkeds = changes.checkedIds;
      this.extCheckListener.next({
        previous: checkeds.previousValue instanceof Array ? checkeds.previousValue : [],
        current: checkeds.currentValue instanceof Array ? checkeds.currentValue : [],
      });

    }
  }

  ngOnInit() {
    this.checkListener.pipe(debounceTime(500)).subscribe(items => this.onClick.emit({ content: this.content, items }));
    if (this.onColorChange.observers.length > 0) {
      this.colorListener = new Subject();
      this.colorListener.subscribe(items => this.onColorChange.emit({ content: this.content, items }));
    }
  }


  ngAfterViewInit() {
    this.checkboxGQL.getAllGeocamada().subscribe(response => {
      const data: any = {};
      data.dados_monitorados = response[0];

      const len = this.content.options.length;

      for (let i = 0; i < len; i++) {
        this.options.push(data[this.content.options[i]]);
      }

      // FLAG VINDO DO INPUT INDICANDO SE DEVE MOSTRAR EXPANDIDO OU NÃO
      // APENAS PARA OS PAIS
      // this.options[0].expanded = this.expanded;

      // Evento sinalizando seu total carregamento
      // Usado no form de Agências
      if (this.onLoad.observers.length > 0) {
        this.onLoad.emit();
      }
    });
  }

  /**
   * Separa o objeto do índice 'Elementos Monitorados' para
   * tratá-lo posteriormente em injetarContagemElementos()
   * @param [Object] contagemElementos
   * objeto da API contendo id e quantidade de cada elemento
   */

  prepararContagemElementos(contagemElementos: any) {
    const contagemElementosData = Array.from(Object.keys(contagemElementos), k => contagemElementos[k]);
    const findElementos = this.options.find(
      elementosMonitorados => elementosMonitorados.nome === 'Elementos Monitorados',
    );
    if (findElementos !== undefined) {
      const elementosMonitorados = findElementos.geocamadas_filhas;

      elementosMonitorados.map(opcao => {
        this.injetarContagemElementos(opcao, contagemElementosData);
      });

      this.somarElementos(elementosMonitorados);
    }
  }

  /**
   * Método para injetar quantidades de elementos da concessionária
   * em seus respectivos índices dos checkboxes
   * @param [Object] opcao [Índice do elemento no checkbox]
   * @param {[Object]} contagemElementos [objeto da API contendo id e quantidade de cada elemento]
   */
  private injetarContagemElementos(opcao, contagemElementosData) {
    // Limpar quantidade de todas as geocamadas
    opcao.qtde = '';
    opcao.geocamadas_filhas.filter(opcao => opcao !== undefined).forEach(subItem => {
      subItem.qtde = '';
      this.injetarContagemElementos(subItem, contagemElementosData);
    });

    contagemElementosData.map(count => {
      const quantidade = count.quantidade;
      if (count.id_geocamada === opcao.id) {
        opcao.qtde = quantidade;
      }
    });
  }

  private somarElementos(elementosMonitorados) {
    elementosMonitorados.map(opcao => {
      let somaElementosPai = 0;
      let somaElementosFilho = 0;
      let somaElementosNeto = 0;

      if (opcao.geocamadas_filhas.length > 0) {
        opcao.geocamadas_filhas.forEach(filho => {
          const quantidadeFormat = +filho.qtde.replace(/[\(\).]/g, '');
          somaElementosPai += quantidadeFormat;
          if (filho.geocamadas_filhas.length > 0) {
            filho.geocamadas_filhas.forEach(neto => {
              // tslint:disable-next-line: no-shadowed-variable
              const quantidadeFormat = +neto.qtde.replace(/[\(\).]/g, '');
              somaElementosFilho += quantidadeFormat;
              if (neto.geocamadas_filhas.length > 0) {
                neto.geocamadas_filhas.forEach(bisneto => {
                  // tslint:disable-next-line: no-shadowed-variable
                  const quantidadeFormat = +bisneto.qtde.replace(/[\(\).]/g, '');
                  somaElementosNeto += quantidadeFormat;
                  somaElementosFilho += somaElementosNeto;
                });
                neto.qtde = this.separarMilhares(somaElementosNeto);
              }
            });
            filho.qtde = this.separarMilhares(somaElementosFilho);
          }
        });
        opcao.qtde = this.separarMilhares(somaElementosPai + somaElementosFilho);
      }
    });
  }

  private separarMilhares(num) {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
}
