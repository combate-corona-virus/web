import { Observable } from 'rxjs/internal/Observable';
import { pluck } from 'rxjs/internal/operators/pluck';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root',
})
export class CheckboxGQL {

  constructor(private apollo: Apollo) { }

  getAllGeocamadaQuery = gql`
    query getAllGeocamada {
      getAllGeocamada(
        is_pai: true
        orderBy:{
          field: "indice"
          order: DESC
        }
      ){
        id
        icone
        cor
        nome_legivel
        indice
        nome
        geocamadas_filhas {
          id
          icone
          cor
          nome_legivel
          indice
          nome
          geocamadas_filhas {
            id
            icone
            cor
            nome_legivel
            indice
            nome
            geocamadas_filhas {
              id
              icone
              cor
              nome_legivel
              indice
              nome
            }
          }
        }
      }
    }
  `;

  public getAllGeocamada(): Observable<any> {
    return this.apollo.query<any>({
      query: this.getAllGeocamadaQuery,
    }).pipe(pluck('data', 'getAllGeocamada'));
  }
}