import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuarioGQL } from '../usuarios.GQL';
import { TabelaBase } from 'app/classes/TabelaBase';
import { DataTableConfig } from 'app/classes/DataTableConfig';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'covid-usuarios-list',
  templateUrl: './usuarios-list.component.html',
  styleUrls: ['./usuarios-list.component.scss'],
})
export class UsuariosListComponent extends TabelaBase implements OnInit {

  usuarios: any;
  dataTableConfig = new DataTableConfig({ emptyMessage: 'Nenhum Registro Encontrado' });

  constructor(
    router: Router,
    route: ActivatedRoute,
    private fb: FormBuilder,
    private usuarioGQL: UsuarioGQL,
  ) {
    super(router, route);
  }

  ngOnInit() {
    this.form = this.fb.group({});
    this.listObservableCallback = params => this.usuarioGQL.search({ ...params });
    super.initializeComponent();
  }

  onEditUsuario(id) {
    this.router.navigate([`pages/usuarios/edit/${id}`]);
  }
}
