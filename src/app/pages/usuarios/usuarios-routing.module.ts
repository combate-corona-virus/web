import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuariosComponent } from './usuarios.component';


const routes: Routes = [{
  path: '',
  component: UsuariosComponent,
  children: [
    {
      path: 'list',
      loadChildren: () => import('./usuarios-list/usuarios-list.module')
        .then(m => m.UsuariosListModule),
    },
    {
      path: 'add',
      loadChildren: () => import('./usuarios-form/usuarios-form.module')
        .then(m => m.UsuariosFormModule),
    },
    {
      path: ':id',
      children: [
        {
          path: 'edit',
          loadChildren: () => import('./usuarios-form/usuarios-form.module')
            .then(m => m.UsuariosFormModule),
        },
        {
          path: '**',
          redirectTo: 'editar',
          pathMatch: 'full',
        },
      ],
    },
    {
      path: '**',
      redirectTo: 'list',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsuariosRoutingModule { }

export const routedComponents = [
  UsuariosComponent,
];
