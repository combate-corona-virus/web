import { Observable } from 'rxjs/internal/Observable';
import { pluck } from 'rxjs/internal/operators/pluck';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root',
})
export class UsuarioGQL {

  constructor(private apollo: Apollo) { }

  getPaginatedUsuariosQuery = gql`
    query getPaginatedUsuarios($first: Int!, $page: Int) {
      getPaginatedUsuarios(first: $first, page: $page, orderBy: [
      {
        field: "nome"
        order: ASC
      }
    ]) {
        paginatorInfo {
          total
        }
        data {
          id
          nome
          sobrenome
          email
          municipio {
            nome
          }
        }
      }
    }
  `;

  public search(values): Observable<any> {
    return this.apollo.query<any>({
      query: this.getPaginatedUsuariosQuery,
      variables: {
        first: values.limit,
        page: Math.floor((values.offset / values.limit) + 1),
      },
    }).pipe(pluck('data', 'getPaginatedUsuarios'));
  }
}

