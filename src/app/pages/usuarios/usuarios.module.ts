import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule } from '@nebular/theme';

import { UsuariosRoutingModule, routedComponents } from './usuarios-routing.module';
import { UsuarioGQL } from './usuarios.GQL';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    UsuariosRoutingModule,
    NbButtonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    UsuarioGQL,
  ],
})
export class UsuariosModule { }
