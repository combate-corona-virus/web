import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import Map from 'ol/Map';
import { Vector as VectorSource, XYZ } from 'ol/source';
import View from 'ol/View';
import { fromLonLat } from 'ol/proj';
import BingMaps from 'ol/source/BingMaps';
import { NbSidebarService } from '@nebular/theme';
import Control from 'ol/control/Control';
import 'ol/ol.css';
import { unByKey } from 'ol/Observable';
import Overlay from 'ol/Overlay';
import { getArea, getLength } from 'ol/sphere';
import { LineString, Polygon } from 'ol/geom';
import Draw from 'ol/interaction/Draw';
import { Tile as TileLayer, Vector as VectorLayer, Heatmap } from 'ol/layer';
import { Circle as CircleStyle, Fill, Stroke, Style } from 'ol/style';
import GeometryType from 'ol/geom/GeometryType';
import OverlayPositioning from 'ol/OverlayPositioning';
import GeoJSON from 'ol/format/GeoJSON';
import OlVectorLayer from 'ol/layer/Vector';
import OlVectorSource from 'ol/source/Vector';
import Text from 'ol/style/Text';
import { MapService } from 'app/providers/services/map.service';
import { Subscription } from 'rxjs';
import { MapsGQL } from './maps.GQL';
import * as proj from 'ol/proj';
import { AuthService } from 'app/providers/services/auth.service';
import Zoom from 'ol/control/Zoom';

declare var jQuery: any;

@Component({
  selector: 'covid-maps',
  template: `
    <div id="map" class="map">
    <div id="popover"></div>

      <div id="indicador-info" class="indicador-info-control ol-zoom ol-unselectable ol-control">
        <div><i class="fa fa-map-marker"></i> Município : {{  municipio }} - {{ estado }} |</div>
        <div><i class="fas fa-history"></i> Atualizado em {{ data_atualizada | date: 'dd/MMMM/yyyy HH:mm:ss' }}</div>
        <div *ngIf="idCamadas.indexOf('13') != -1">
          <i class="far fa-map"></i> Mapa de incidência - Casos suspeitos </div>
        <div *ngIf="idCamadas.indexOf('15') != -1">
          <i class="far fa-map"></i> Mapa de incidência - Casos confirmados </div>
      </div>

      <div id="indicador-posicao" class="indicador-posicao-control ol-zoom ol-unselectable ol-control">
        <button (click)="openCamadas()" class="generic-tooltip" title="Clique para exibir as camadas">
        <i class="fa fa-layer-group"></i> Camadas
        </button>
      </div>

      <div id="indicador-polygon" class="indicador-polygon-control ol-zoom ol-unselectable ol-control">
        <button (click)="onPolygonDraw()" class="generic-tooltip" title="Clique para desenhar Área (Polígono).">
          <i class="fa fa-draw-polygon"></i>
        </button>
      </div>

      <div id="indicador-line" class="indicador-line-control ol-zoom ol-unselectable ol-control">
        <button (click)="onLineDraw()" class="generic-tooltip" title="Clique para desenha Comprimento (Linha).">
          <i class="fa fa-pencil-ruler"></i>
        </button>
      </div>

      <div id="indicador-clear" class="indicador-clear-control ol-zoom ol-unselectable ol-control">
        <button (click)="clear()" class="generic-tooltip" title="Clique para apagar os desenhos feitos no mapa.">
          <i class="fa fa-eraser"></i>
        </button>
      </div>

      <div id="download-png" class="download-png-control ol-zoom ol-unselectable ol-control">
        <button (click)="downloadMapPng()" class="generic-tooltip" title="Clique para gerar uma imagem .png do mapa.">
        <i class="fas fa-download"></i>
        </button>
      </div>
      <a id="image-download" download="map.png"></a>
    </div>
  `,
  styleUrls: ['./maps.component.scss'],
})
export class MapsComponent implements OnInit, AfterViewInit, OnDestroy {

  public map: Map;
  public sketch;
  public helpTooltipElement;
  public helpTooltip: Overlay = new Overlay({});
  public measureTooltipElement;
  public measureTooltip: Overlay = new Overlay({});
  public continuePolygonMsg = 'Clique para continuar desenhando o polígono';
  public continueLineMsg = 'Clique para continuar desenhando a linha';

  public draw;
  public source: VectorSource;
  public pointerMoveHandler;
  public ativoDrawPolygon: boolean = false;
  public ativoDrawLineString: boolean = false;

  public vector;
  public popup: Overlay;
  public camadasSubscribe: Subscription;
  public idCamadas = [];
  public municipio = AuthService.getToken().municipio.nome;
  // tslint:disable-next-line: max-line-length
  public estado = AuthService.getToken().municipio.unidadeFederativa ? AuthService.getToken().municipio.unidadeFederativa.nome : '';
  public data_atualizada = '';

  constructor(
    private sidebarService: NbSidebarService,
    private mapService: MapService,
    private mapsGQL: MapsGQL,
  ) { }

  ngOnInit() {
    this.camadasSubscribe = this.mapService.ids_camadas.subscribe(
      (response: any) => {
        const camadaRemovida = this.idCamadas.filter(x => !response.includes(x));
        if (camadaRemovida.length > 0) {
          camadaRemovida.forEach(id => {
            this.onRemoveCamadaMap(id);
          });
        }

        this.idCamadas = response;

        if (this.idCamadas.length > 0) {
          const currentExtent = this.getCurrentExtentLatLon();
          const bounds = {
            x_min: currentExtent[0],
            y_min: currentExtent[1],
            x_max: currentExtent[2],
            y_max: currentExtent[3],
          };
          this.mapsGQL.getFeatureCollection(this.idCamadas, bounds).subscribe(data => {
            const fc = [];
            data.forEach(featureCollection => {
              if (featureCollection.features.length > 0) {
                const obj: any = {};
                obj.fc = featureCollection;
                fc.push(obj);
                if ('undefined' !== typeof featureCollection.id_geocamada) {
                  this.onRemoveCamadaMap(featureCollection.id_geocamada);
                }
              }
            });
            if (fc.length > 0) {
              this.addGeoJSON(fc);
            }
          },
          );
        }
      },
    );

    this.mapsGQL.getDataUltimaImportacao([AuthService.getToken().id_municipio]).subscribe(
      response => response.length > 0 ? this.data_atualizada = response[0].created_at : this.data_atualizada = '',
    );
  }

  ngAfterViewInit() {

    const raster = new TileLayer({
      visible: true,
      preload: Infinity,
      // source: new BingMaps({
      //   key:
      //     'AoGwOff1xHlD_EsIyFAVwedXJpeC5RObYdYWjtC8xULKeMqhyRT3x4dXZJQg-n2n',
      //   imagerySet: 'Road',
      //   maxZoom: 19,
      // }),
      source: new XYZ({
        url: 'http://mt{0-3}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
      }),
    });

    this.source = new VectorSource();

    const vector = new VectorLayer({
      source: this.source,
      style: new Style({
        fill: new Fill({
          color: 'rgba(49, 112, 88, 0.2)',
        }),
        stroke: new Stroke({
          color: '#317058',
          width: 2,
        }),
        image: new CircleStyle({
          radius: 7,
          fill: new Fill({
            color: '#317058',
          }),
        }),
      }),
    });

    const minusZoomIcon = document.createElement('i');
    const plusZoomIcon = document.createElement('i');
    plusZoomIcon.className = 'fa-search-plus fa';
    minusZoomIcon.className = 'fa-search-minus fa';

    this.map = new Map({
      layers: [raster, vector],
      target: 'map',
      view: new View({
        center: fromLonLat(AuthService.getToken().municipio.geom_centro.coordinates),
        zoom: 13,
        minZoom: 12,
        maxZoom: 14,
      }),
      controls: [
        new Control({
          element: document.getElementById('indicador-info'),
        }),
        new Control({
          element: document.getElementById('indicador-posicao'),
        }),
        new Control({
          element: document.getElementById('indicador-polygon'),
        }),
        new Control({
          element: document.getElementById('indicador-line'),
        }),
        new Control({
          element: document.getElementById('indicador-clear'),
        }),
        new Control({
          element: document.getElementById('download-png'),
        }),
        new Zoom({
          zoomInLabel: plusZoomIcon,
          zoomInTipLabel: 'Zoom in (aproximar). Atalho: Duplo clique',
          zoomOutLabel: minusZoomIcon,
          zoomOutTipLabel: 'Zoom out (afastar). Atalho: Shift + Duplo clique',
        }),
      ],
    });

    setTimeout(() => {
      this.map.updateSize();
    }, 300);

    this.map.on('moveend', () => {

      if (this.idCamadas.length > 0) {
        const currentExtent = this.getCurrentExtentLatLon();
        const bounds = {
          x_min: currentExtent[0],
          y_min: currentExtent[1],
          x_max: currentExtent[2],
          y_max: currentExtent[3],
        };

        this.mapsGQL.getFeatureCollection(this.idCamadas, bounds).subscribe(data => {
          const fc = [];
          data.forEach(featureCollection => {
            if (featureCollection.features.length > 0) {
              const obj: any = {};
              obj.fc = featureCollection;
              fc.push(obj);
              if ('undefined' !== typeof featureCollection.id_geocamada) {
                this.onRemoveCamadaMap(featureCollection.id_geocamada);
              }
            }
          });
          if (fc.length > 0) {
            this.addGeoJSON(fc);
          }
        },
        );
      }
    });

    this.onMapEvent();
  }

  public onMapEvent() {

    this.popup = new Overlay({
      element: document.getElementById('popover'),
    });
    this.map.addOverlay(this.popup);

    let oldFeature;
    this.map.on('pointermove', (evt) => {
      if (evt.dragging)
        return;

      const element = this.popup.getElement();
      const feature: any = this.map.forEachFeatureAtPixel(evt.pixel,
        // tslint:disable-next-line: no-shadowed-variable
        function (feature) {
          return feature;
        });

      if (feature) {
        if (feature !== oldFeature) {

          if (typeof feature.get('features') === 'undefined') {
            const prop = feature.getProperties();
            // tslint:disable-next-line: triple-equals
            if (prop.id_geocamada == 13 || prop.id_geocamada == 15) {
              return;
            }
            if (prop.id) {
              const coordinates = feature.getGeometry().getCoordinates();
              this.popup.setPosition(coordinates);

              let content: string = `<b>Faixa etária</b>: ${prop.faixa_etaria}<br>`;
              content += `<b>Gênero</b>: ${prop.sexo}<br>`;
              content += `<b>Nº Notificação</b>: ${prop.numero_notificacao}<br>`;
              content += `<b>Condição</b>: ${prop.condicao}<br>`;
              content += `<b>Status Exame</b>: ${prop.status_exame}<br>`;

              jQuery(element).popover({
                placement: 'top',
                animation: false,
                html: true,
                title: `<strong>${prop.nome_legivel} #${prop.id}</strong>`,
                content: content,
                template: `
                <div class="popover" role="tooltip">
                  <div class="popover-header"></div>
                  <div class="popover-body"></div>
                </div>
                `,
              });

              jQuery(element).popover('show');
              oldFeature = feature;
            }
          }
        }
      } else {
        jQuery(element).popover('dispose');
        oldFeature = null;
      }
    });
  }

  public addInteraction(type) {

    this.pointerMoveHandler = (evt) => {
      if (evt.dragging) {
        return;
      }

      let helpMsg = 'Clique para começar a desenhar';

      if (this.sketch) {
        const geom = this.sketch.getGeometry();
        if (geom instanceof Polygon) {
          helpMsg = this.continuePolygonMsg;
        } else if (geom instanceof LineString) {
          helpMsg = this.continueLineMsg;
        }
      }

      this.helpTooltipElement.innerHTML = helpMsg;
      this.helpTooltip.setPosition(evt.coordinate);
      this.helpTooltipElement.classList.remove('hidden');
    };

    this.draw = new Draw({
      source: this.source,
      type: type,
      style: new Style({
        fill: new Fill({
          color: 'rgba(49, 112, 88, 0.2)',
        }),
        stroke: new Stroke({
          color: 'rgba(128, 178, 90, 0.5)',
          lineDash: [10, 10],
          width: 2,
        }),
        image: new CircleStyle({
          radius: 5,
          stroke: new Stroke({
            color: 'rgba(49, 112, 88, 0.6)',
          }),
          fill: new Fill({
            color: 'rgba(49, 112, 88, 0.2)',
          }),
        }),
      }),
    });

    this.map.addInteraction(this.draw);
    this.createMeasureTooltip();
    this.createHelpTooltip();

    let listener;
    this.draw.on('drawstart',
      (evt) => {
        this.sketch = evt.feature;

        let tooltipCoord = evt.coordinate;

        listener = this.sketch.getGeometry().on('change',
          // tslint:disable-next-line: no-shadowed-variable
          (evt) => {
            const geom = evt.target;
            let output;
            if (geom instanceof Polygon) {
              output = this.formatArea(geom);
              tooltipCoord = geom.getInteriorPoint().getCoordinates();
            } else if (geom instanceof LineString) {
              output = this.formatLength(geom);
              tooltipCoord = geom.getLastCoordinate();
            }
            this.measureTooltipElement.innerHTML = output;
            this.measureTooltip.setPosition(tooltipCoord);
          });
      });

    this.draw.on('drawend',
      () => {
        this.measureTooltipElement.className = 'ol-tooltip ol-tooltip-static';
        this.measureTooltip.setOffset([0, -7]);
        this.sketch = null;
        this.measureTooltipElement = null;
        this.createMeasureTooltip();
        unByKey(listener);
      });

    this.map.on('pointermove', this.pointerMoveHandler);
    this.map.getViewport().addEventListener('mouseout', () => {
      this.helpTooltipElement.classList.add('hidden');
    });
  }

  public createHelpTooltip() {
    if (this.helpTooltipElement) {
      this.helpTooltipElement.parentNode.removeChild(this.helpTooltipElement);
    }
    this.helpTooltipElement = document.createElement('div');
    this.helpTooltipElement.className = 'ol-tooltip hidden';
    this.helpTooltip = new Overlay({
      element: this.helpTooltipElement,
      offset: [15, 0],
      positioning: OverlayPositioning.CENTER_LEFT,
    });
    this.map.addOverlay(this.helpTooltip);
  }

  public createMeasureTooltip() {
    if (this.measureTooltipElement) {
      this.measureTooltipElement.parentNode.removeChild(this.measureTooltipElement);
    }
    this.measureTooltipElement = document.createElement('div');
    this.measureTooltipElement.className = 'ol-tooltip ol-tooltip-measure';
    this.measureTooltip = new Overlay({
      element: this.measureTooltipElement,
      offset: [0, -15],
      positioning: OverlayPositioning.BOTTOM_CENTER,
    });
    this.map.addOverlay(this.measureTooltip);
  }

  public formatLength(line) {
    const length = getLength(line);
    let output;
    if (length > 100) {
      output = (Math.round(length / 1000 * 100) / 100) +
        ' ' + 'km';
    } else {
      output = (Math.round(length * 100) / 100) +
        ' ' + 'm';
    }
    return output;
  }

  public formatArea(polygon) {
    const area = getArea(polygon);
    let output;
    if (area > 10000) {
      output = (Math.round(area / 1000000 * 100) / 100) +
        ' ' + 'km<sup>2</sup>';
    } else {
      output = (Math.round(area * 100) / 100) +
        ' ' + 'm<sup>2</sup>';
    }
    return output;
  }

  public onPolygonDraw() {
    if (this.ativoDrawLineString) {
      this.ativoDrawLineString = false;
      this.map.removeInteraction(this.draw);
      this.map.un('pointermove', this.pointerMoveHandler);
    }

    if (this.ativoDrawPolygon) {
      this.helpTooltipElement.innerHTML = null;
      this.map.removeInteraction(this.draw);
      this.map.un('pointermove', this.pointerMoveHandler);
      this.ativoDrawPolygon = false;
    } else {
      this.addInteraction(GeometryType.POLYGON);
      this.ativoDrawPolygon = true;
    }
  }

  public onLineDraw() {
    if (this.ativoDrawPolygon) {
      this.ativoDrawPolygon = false;
      this.map.removeInteraction(this.draw);
      this.map.un('pointermove', this.pointerMoveHandler);
    }

    if (this.ativoDrawLineString) {
      this.helpTooltipElement.innerHTML = null;
      this.map.removeInteraction(this.draw);
      this.map.un('pointermove', this.pointerMoveHandler);
      this.ativoDrawLineString = false;
    } else {
      this.addInteraction(GeometryType.LINE_STRING);
      this.ativoDrawLineString = true;
    }
  }

  public clear() {
    this.source.clear();
    this.map.getOverlays().clear();
    this.map.addOverlay(this.helpTooltip);
    this.map.addOverlay(this.measureTooltip);
    this.map.addOverlay(this.popup);
  }

  public openCamadas() {
    this.sidebarService.toggle(false, 'settings-sidebar');
    this.map.updateSize();
  }

  addGeoJSON(data: any[]): boolean {
    const zoom = this.map.getView().getZoom();

    if (!data) {
      return false;
    }
    const len = data.length;
    for (let i = 0; i < len; i++) {

      const features = (new GeoJSON()).readFeatures(data[i].fc, {
        featureProjection: 'EPSG:3857',
      });

      // tslint:disable-next-line: triple-equals
      if (data[i].fc.id_geocamada == 13 || data[i].fc.id_geocamada == 15) {
        const heatMapLayer = new Heatmap({
          source: new OlVectorSource({
            features,
          }),
          radius: 12,
          blur: 20,
        });
        heatMapLayer.setProperties({ id_geocamada: data[i].fc.id_geocamada });
        this.map.addLayer(heatMapLayer);

      } else {
        let pins = new OlVectorLayer({});

        if (features[0].getGeometry().getType() === 'Point') {
          pins = new OlVectorLayer({
            source: new OlVectorSource({
              features,
            }),
            style: (feature) => {
              const prop = feature.getProperties();
              const color = prop.cor;

              const style = new Style({
                text: new Text({
                  text: String.fromCharCode(parseInt(prop.icone_unicode, 16)),
                  font: '900 18px "Font Awesome 5 Free"',
                  scale: 1.1,
                  textAlign: 'center',
                  fill: new Fill({
                    color,
                  }),
                }),
              });
              return style;
            },
          });
        } else {
          pins = new OlVectorLayer({
            source: new OlVectorSource({
              features,
            }),
            style: (feature) => {
              const prop = feature.getProperties();
              const color = prop.cor;

              const lightColor = this.convertHexToRGBA(color, 55);
              const noColor = this.convertHexToRGBA(color, 0);

              const style = new Style({
                fill: new Fill({
                  color: lightColor,
                }),
                stroke: new Stroke({
                  color: noColor,
                  lineDash: [10, 10],
                  width: 2,
                }),
                image: new CircleStyle({
                  radius: 5,
                  stroke: new Stroke({
                    color: lightColor,
                  }),
                  fill: new Fill({
                    color: lightColor,
                  }),
                }),
              });
              return style;
            },
          });
        }
        pins.setProperties({ id_geocamada: data[i].fc.id_geocamada });
        this.map.addLayer(pins);
      }

    }
    return true;
  }

  public convertHexToRGBA(color: string, opacity: number): string {
    if (color.indexOf('#') === -1) {
      return color;
    }

    color = color.replace('#', '');
    const r = parseInt(color.substring(0, 2), 16);
    const g = parseInt(color.substring(2, 4), 16);
    const b = parseInt(color.substring(4, 6), 16);

    return `rgba(${r},${g},${b},${opacity / 100})`;
  }

  public onRemoveCamadaMap(id_camada: number) {
    this.map.getLayers().getArray()
      // tslint:disable-next-line: triple-equals
      .filter(layer => layer.get('id_geocamada') == id_camada)
      .forEach(layer => this.map.removeLayer(layer));
  }

  /**
   * Calcula extent da view atual do mapa. SRID 3857
   *
   * @return {Array<number>} [minX, minY, maxX, maxY]
   * minX, minY - Canto inferior esquerdo
   * maxX, maxY - Canto superior direito
   */
  public getCurrentExtent() {
    return this.map.getView().calculateExtent(this.map.getSize());
  }


  /**
   * Calcula extent da view atual do mapa. SRID 4326
   *
   * @return {Array<number>} [minX, minY, maxX, maxY]
   */
  public getCurrentExtentLatLon(): number[] {
    const extent = this.getCurrentExtent();
    return proj.transformExtent(extent, 'EPSG:3857', 'EPSG:4326');
  }

  public downloadMapPng() {
    this.map.once('rendercomplete', () => {
      const mapCanvas: any = document.createElement('canvas');
      const size = this.map.getSize();
      mapCanvas.width = size[0];
      mapCanvas.height = size[1];
      const mapContext = mapCanvas.getContext('2d');
      Array.prototype.forEach.call(document.querySelectorAll('.ol-layer canvas'), function (canvas) {
        if (canvas.width > 0) {
          const opacity = canvas.parentNode.style.opacity;
          mapContext.globalAlpha = opacity === '' ? 1 : Number(opacity);
          const transform = canvas.style.transform;
          const matrix = transform.match(/^matrix\(([^\(]*)\)$/)[1].split(',').map(Number);
          CanvasRenderingContext2D.prototype.setTransform.apply(mapContext, matrix);
          mapContext.drawImage(canvas, 0, 0);
        }
      });
      if (navigator.msSaveBlob) {
        navigator.msSaveBlob(mapCanvas.msToBlob(), 'map.png');
      } else {
        const link: any = document.getElementById('image-download');
        link.href = mapCanvas.toDataURL();
        link.click();
      }
    });
    this.map.renderSync();
  }

  ngOnDestroy() {
    this.camadasSubscribe.unsubscribe();
    this.sidebarService.collapse('settings-sidebar');
  }
}
