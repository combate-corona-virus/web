import { Observable } from 'rxjs/internal/Observable';
import { pluck } from 'rxjs/internal/operators/pluck';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root',
})
export class MapsGQL {

  constructor(private apollo: Apollo) { }

  getFeatureCollectionQuery = gql`
    query getFeatureCollection($id_geocamada: [Int], $bounds: Bounds) {
      getFeatureCollection(id_geocamada: $id_geocamada, bounds: $bounds){
        type
        id_geocamada
        features {
          type
          geometry
          properties
        }
      }
    }
  `;

  getDataUltimaImportacaoQuery = gql`
  query getPaginatedPaciente($id_municipio: [Int]) {
    getPaginatedPaciente(
      first:1
      filter: {
        id_municipio: $id_municipio
        orderBy:{
          field: "created_at"
          order: DESC
      }})
      {
        data {
          created_at
        }
      }
    }
  `;

  public getFeatureCollection(id_geocamada: number[], bounds: any): Observable<any> {
    return this.apollo.query<any>({
      query: this.getFeatureCollectionQuery,
      variables: {
        id_geocamada,
        bounds,
      },
    }).pipe(pluck('data', 'getFeatureCollection'));
  }

  public getDataUltimaImportacao(id_municipio: number[]): Observable<any> {
    return this.apollo.query<any>({
      query: this.getDataUltimaImportacaoQuery,
      variables: {
        id_municipio,
      },
    }).pipe(pluck('data', 'getPaginatedPaciente', 'data'));
  }
}

