import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaqListComponent } from './faq-list/faq-list.component';
import { FaqComponent } from './faq.component';


const routes: Routes = [{
  path: '',
  component: FaqComponent,
  children: [
    {
      path: 'list',
      component: FaqListComponent,
    },
    {
      path: '**',
      redirectTo: 'list',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FaqRoutingModule { }

export const routedComponents = [
  FaqComponent,
  FaqListComponent,
];
