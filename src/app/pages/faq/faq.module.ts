import { NgModule } from '@angular/core';
import {
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbTreeGridModule,
  NbButtonModule,
  NbAccordionModule,
} from '@nebular/theme';

import { FaqRoutingModule, routedComponents } from './faq-routing.module';

import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    FaqRoutingModule,
    TableModule,
    ButtonModule,
    NbButtonModule,
    NbAccordionModule,
  ],
  declarations: [...routedComponents],
})
export class FaqModule {}
