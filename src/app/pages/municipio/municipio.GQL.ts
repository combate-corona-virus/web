import { Observable } from 'rxjs/internal/Observable';
import { pluck } from 'rxjs/internal/operators/pluck';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root',
})
export class MunicipioGQL {

  constructor(private apollo: Apollo) { }

  updateMunicipioMutation = gql`
    mutation updateMunicipio($input: UpdateMunicipioInput!) {
      updateMunicipio(input: $input) {
        id
        texto_contato
        nome_prefeitura
        logo {
          url
          caminho
        }
      }
    }
  `;

  public updateMunicipio(input: any): Observable<any> {
    return this.apollo.mutate<any>({
      mutation: this.updateMunicipioMutation,
      variables: {
        input,
      },
    }).pipe(pluck('data', 'updateMunicipio'));
  }
}

