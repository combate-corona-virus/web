import { NgModule } from '@angular/core';
import {
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbTreeGridModule,
  NbButtonModule,
  NbTooltipModule,
} from '@nebular/theme';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';

import { MunicipioRoutingModule, routedComponents } from './municipio-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'primeng/fileupload';
import { ImportacaoService } from 'app/providers/services/importacao.service';
import { MunicipioGQL } from './municipio.GQL';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    MunicipioRoutingModule,
    TableModule,
    ButtonModule,
    NbTooltipModule,
    NbButtonModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    CommonModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    ImportacaoService,
    MunicipioGQL,
  ],
})
export class MunicipioModule { }
