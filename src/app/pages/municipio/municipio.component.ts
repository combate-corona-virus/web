import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ImportacaoService } from 'app/providers/services/importacao.service';
import { MunicipioGQL } from './municipio.GQL';
import { LoggedUserService } from 'app/providers/services/logged-user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'covid-municipio',
  templateUrl: './municipio.component.html',
  styleUrls: ['./municipio.component.scss'],
})
export class MunicipioComponent implements OnInit {

  form: FormGroup;
  uploadedFile: File = null;

  nomePrefeitura: string;
  url: string;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private municipioGQL: MunicipioGQL,
    private loggedUserService: LoggedUserService,
    private importacaoService: ImportacaoService,
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'id': [null],
      'texto_contato' : [null, Validators.required],
    });

    this.loggedUserService.userInfoAsync.subscribe(userInfo => {
      this.nomePrefeitura = userInfo.municipio.nome_prefeitura;

      if (userInfo.municipio.logo) {
        this.url = userInfo.municipio.logo.url;
      }
      if (userInfo.municipio.texto_contato != null) {
        this.form.patchValue({
          texto_contato: userInfo.municipio.texto_contato,
          id: userInfo.municipio.id,
        });
      } else {
        this.form.patchValue({
          id: userInfo.municipio.id,
          // tslint:disable-next-line: max-line-length
          texto_contato: `Neste momento contamos com todos para garantir a saúde, a segurança e o bem-estar de todos.\n\nFale com a Prefeitura\n${this.nomePrefeitura}: 14 3235-1000\nOuvidoria municipal: 14 3235-1000\nDisk-COVID: 192`,
        });
      }
    });
  }

  onSubmit() {
    if (!this.form.valid) {
      Swal.fire(
        'Atenção',
        'Existem campos inválidos. Verifique novamente os campos marcados.',
        'info',
      );
      return;
    }
    this.municipioGQL.updateMunicipio(this.form.value).subscribe(
      () => {
        Swal.fire(
          'Atualizado!',
          'Informações atualizadas com sucesso.',
          'success',
        );
        this.router.navigate(['pages/indicadores']);
      },
    );

    if (this.uploadedFile != null) {
      const file = this.uploadedFile;
      this.uploadedFile = null;
      this.importacaoService.uploadLogo(file).subscribe(
        () => {
          Swal.fire(
            'Logo importado!',
            'Importação do Logo realizado com sucesso.',
            'success',
          );
          this.router.navigate(['pages/indicadores']);
        },
        () => {
          Swal.fire(
            'Erro no upload do Logo!',
            `Tente novamente.`,
            'error',
          );
        },
      );
    }
  }

  public onSelectImage(evt: any) {
    this.uploadedFile = evt[0];
  }
}
