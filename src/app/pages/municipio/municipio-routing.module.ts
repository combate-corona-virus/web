import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MunicipioComponent } from './municipio.component';

const routes: Routes = [
  {
    path: '',
    component: MunicipioComponent,
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MunicipioRoutingModule { }

export const routedComponents = [
  MunicipioComponent,
];
