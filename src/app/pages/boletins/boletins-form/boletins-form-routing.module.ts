import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BoletinsFormComponent } from './boletins-form.component';

const routes: Routes = [
  {
  path: '',
  component: BoletinsFormComponent,
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BoletinsFormRoutingModule { }

export const routedComponents = [
  BoletinsFormComponent,
];
