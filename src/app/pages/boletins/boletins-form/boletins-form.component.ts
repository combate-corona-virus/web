import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterHelper } from 'app/providers/utils/router-helper.util';
import { LoggedUserService } from 'app/providers/services/logged-user.service';
import { BoletimGQL } from '../boletim.GQL';
import Swal from 'sweetalert2';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'covid-boletim-form',
  templateUrl: './boletins-form.component.html',
  styleUrls: [ './boletins-form.component.scss'],
})
export class BoletinsFormComponent  implements OnInit {

  form: FormGroup;
  userMunicipio: number;
  resolvedBoletim: any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private boletimGQL: BoletimGQL,
    private router: Router,
    private loggedUserService: LoggedUserService,
  ) { }

  ngOnInit() {
    this.loggedUserService.userInfoAsync.subscribe(userInfo => {
      this.userMunicipio = userInfo.id_municipio;
    });

    RouterHelper.getResolvedData(this.route, 'boletim').subscribe(boletim => {
      this.resolvedBoletim = boletim;
    });

    this.form = this.formBuilder.group({
      'data': [null, Validators.required],
      'quantidade_suspeitos': [null, Validators.required],
      'quantidade_confirmados': [null, Validators.required],
      'quantidade_obitos': [null, Validators.required],
      'quantidade_descartados': [null, Validators.required],
      'quantidade_curados': [null, Validators.required],
      'quantidade_obitos_suspeitos': [null, Validators.required],

      'quantidade_confirmados_previsto_fiocruz': [null],
      'quantidade_testagem': [null],
      'quantidade_leitos_uti': [null],
      'quantidade_leitos_uti_ocupados': [null],
      'indice_isolamento_social': [null],
    }, {});

    if (this.resolvedBoletim) {
      this.form.patchValue(this.resolvedBoletim);
    }
  }

  onSubmit(formValue: any) {
    formValue.id_municipio = this.userMunicipio;
    this.resolvedBoletim ? this.updateBoletim(formValue) : this.createBoletim(formValue);
  }

  createBoletim(formValue: any): void {
    this.boletimGQL.create(formValue).subscribe(() => {
      this.boletimGQL.search({ limit: 30, offset: 0, refetchQuery: true }, this.userMunicipio).subscribe(() => {
        this.router.navigate([`pages/boletins/list`]);
        Swal.fire(
          'Criado',
          'O boletim foi criado com sucesso.',
          'success',
        );
      });
    });
  }

  updateBoletim(formValue: any): void {
    this.boletimGQL.update(this.resolvedBoletim.id, formValue).subscribe(() => {
      this.router.navigate([`pages/boletins/list`]);
      Swal.fire(
        'Atualizado!',
        'O boletim foi atulizado com sucesso.',
        'success',
      );
    });
  }
}
