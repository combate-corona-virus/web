import { NgModule } from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from '@nebular/theme';
import { FormsModule as ngFormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThemeModule } from '../../../@theme/theme.module';
import { CommonModule } from '@angular/common';
import {FileUploadModule} from 'primeng/fileupload';
import { SomaCalendarModule } from 'app/components/soma-calendar/soma-calendar.module';

import { BoletinsFormRoutingModule, routedComponents } from './boletins-form-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BoletinsFormRoutingModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    FileUploadModule,
    SomaCalendarModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class BoletinsFormModule { }
