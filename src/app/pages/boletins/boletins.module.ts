import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BoletimResolver } from 'app/providers/services/boletim-resolver.service';
import { BoletinsRoutingModule, routedComponents } from './boletins-routing.module';
import { BoletimGQL } from './boletim.GQL';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BoletinsRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    BoletimGQL,
    BoletimResolver,
  ],
})
export class BoletinsModule { }
