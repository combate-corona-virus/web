import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BoletimResolver } from 'app/providers/services/boletim-resolver.service';
import { BoletinsComponent } from './boletins.component';

const routes: Routes = [
  {
    path: '',
    component: BoletinsComponent,
    children: [
      {
        path: 'list',
        loadChildren: () => import('./boletins-list/boletins-list.module')
          .then(m => m.BoletinsListModule),
      },
      {
        path: 'add',
        loadChildren: () => import('./boletins-form/boletins-form.module')
          .then(m => m.BoletinsFormModule),
      },
      {
        path: ':id',
        resolve: { boletim: BoletimResolver },
        children: [
          {
            path: 'edit',
            loadChildren: () => import('./boletins-form/boletins-form.module')
              .then(m => m.BoletinsFormModule),
          },
          {
            path: '**',
            redirectTo: 'edit',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: '**',
        redirectTo: 'list',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BoletinsRoutingModule { }

export const routedComponents = [
  BoletinsComponent,
];
