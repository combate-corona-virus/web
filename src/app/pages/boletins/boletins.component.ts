import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'covid-boletins',
  template: `<router-outlet></router-outlet>`,
})
export class BoletinsComponent { }
