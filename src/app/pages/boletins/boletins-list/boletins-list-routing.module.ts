import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BoletinsListComponent } from './boletins-list.component';

const routes: Routes = [
  {
  path: '',
  component: BoletinsListComponent,
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BoletinsListRoutingModule { }

export const routedComponents = [
    BoletinsListComponent,
];
