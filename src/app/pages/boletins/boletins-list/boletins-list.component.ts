import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { map, switchMap } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { DataTableConfig } from 'app/classes/DataTableConfig';
import { TabelaBase } from 'app/classes/TabelaBase';
import { LoggedUserService } from 'app/providers/services/logged-user.service';

import { BoletimGQL } from '../boletim.GQL';

@Component({
  selector: 'covid-boletins-list',
  templateUrl: './boletins-list.component.html',
  styleUrls: ['./boletins-list.component.scss'],
})
export class BoletinsListComponent extends TabelaBase implements OnInit {

  boletins: any;
  userMunicipio: number;
  dataTableConfig = new DataTableConfig({ emptyMessage: 'Nenhum Registro Encontrado' });

  constructor(
    router: Router,
    route: ActivatedRoute,
    private fb: FormBuilder,
    private boletimGQL: BoletimGQL,
    private loggedUserService: LoggedUserService,
  ) {
    super(router, route);
  }

  ngOnInit() {
    this.loggedUserService.userInfoAsync.subscribe(userInfo => {
      this.userMunicipio = userInfo.id_municipio;
    });

    this.form = this.fb.group({});
    this.listObservableCallback = params => this.boletimGQL.search({ ...params }, this.userMunicipio);
    super.initializeComponent();
  }

  excluir(boletim: any): void {
    Swal.fire({
      title: 'Deseja excluir o boletim?',
      text: 'O boletim será excluído e não será possível reverter essa operação pelo sistema.',
      icon: 'question',
      showCancelButton: true,
      cancelButtonColor: 'rgb(170, 170, 170)',
      confirmButtonColor: 'rgb(207, 52, 39)',
      cancelButtonText: 'Não, cancelar',
      confirmButtonText: 'Sim, excluir',
      reverseButtons: true,
    }).then((result) => {
      if (result.value) {
        this.boletimGQL.delete(boletim.id).pipe(
          map(() => this.mergeQueryParams(this.queryParamsConfig, 'refetchQuery')),
          switchMap(this.listObservableCallback),
        ).subscribe(searchResponse => {
          this.collection = searchResponse.data;
          this.totalRecords = searchResponse.paginatorInfo.total;
          Swal.fire(
            'Excluído!',
            'O boletim foi excluído com sucesso.',
            'success',
          );
        });
      }
    });
  }

}
