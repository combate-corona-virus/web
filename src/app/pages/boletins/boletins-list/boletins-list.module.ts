import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule } from '@nebular/theme';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';

import { BoletinsListRoutingModule, routedComponents } from './boletins-list-routing.module';

@NgModule({
  imports: [
    CommonModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    BoletinsListRoutingModule,
    TableModule,
    ButtonModule,
    NbButtonModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class BoletinsListModule { }
