import { Observable } from 'rxjs/internal/Observable';
import { pluck } from 'rxjs/internal/operators/pluck';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root',
})
export class BoletimGQL {

  constructor(private apollo: Apollo) { }

  getPaginatedBoletimEpidemiologicoQuery = gql`
    query getPaginatedBoletimEpidemiologico($first: Int!, $page: Int, $id_municipio: [Int]) {
      getPaginatedBoletimEpidemiologico(first: $first, page: $page, id_municipio: $id_municipio, orderBy: [
        {
          field: "data"
          order: DESC
        }
      ]) {
        paginatorInfo {
          total
        }
        data {
          id
          created_at
          quantidade_suspeitos
          quantidade_confirmados
          quantidade_obitos
          quantidade_descartados
          quantidade_curados
          quantidade_obitos_suspeitos
          quantidade_confirmados_previsto_fiocruz
          quantidade_testagem
          quantidade_leitos_uti
          quantidade_leitos_uti_ocupados
          indice_isolamento_social
          data
          quantidade_confirmados_previsto_fiocruz
          quantidade_testagem
          quantidade_leitos_uti
          quantidade_leitos_uti_ocupados
          indice_isolamento_social
          municipio {
            nome
          }
        }
      }
    }
  `;

  getBoletimEpidemiologicoQuery = gql`
    query getBoletimEpidemiologico($id: ID!) {
      getBoletimEpidemiologico(id: $id) {
        id
        created_at
        quantidade_suspeitos
        quantidade_confirmados
        quantidade_obitos
        quantidade_descartados
        quantidade_curados
        quantidade_obitos_suspeitos
        quantidade_confirmados_previsto_fiocruz
        quantidade_testagem
        quantidade_leitos_uti
        quantidade_leitos_uti_ocupados
        indice_isolamento_social
        data
        municipio {
          nome
        }
      }
    }
  `;

  createBoletimEpidemiologicoMutation = gql`
    mutation createBoletimEpidemiologico(
      $data: DateTime,
      $quantidade_suspeitos: Int,
      $quantidade_confirmados: Int,
      $quantidade_obitos: Int,
      $quantidade_descartados: Int,
      $quantidade_curados: Int,
      $quantidade_obitos_suspeitos: Int,
      $quantidade_confirmados_previsto_fiocruz: Int
      $quantidade_testagem: Int
      $quantidade_leitos_uti: Int
      $quantidade_leitos_uti_ocupados: Int
      $indice_isolamento_social: Float
      $id_municipio: Int
      ) {
      createBoletimEpidemiologico(
        data: $data,
        quantidade_suspeitos: $quantidade_suspeitos,
        quantidade_confirmados: $quantidade_confirmados,
        quantidade_obitos: $quantidade_obitos,
        quantidade_descartados: $quantidade_descartados,
        quantidade_curados: $quantidade_curados,
        quantidade_obitos_suspeitos: $quantidade_obitos_suspeitos,
        quantidade_confirmados_previsto_fiocruz: $quantidade_confirmados_previsto_fiocruz,
        quantidade_testagem: $quantidade_testagem,
        quantidade_leitos_uti: $quantidade_leitos_uti,
        quantidade_leitos_uti_ocupados: $quantidade_leitos_uti_ocupados,
        indice_isolamento_social: $indice_isolamento_social,
        id_municipio: $id_municipio
      ) {
        data
        quantidade_suspeitos
        quantidade_confirmados
        quantidade_obitos
        quantidade_descartados
        quantidade_curados
        quantidade_obitos_suspeitos
        quantidade_confirmados_previsto_fiocruz
        quantidade_testagem
        quantidade_leitos_uti
        quantidade_leitos_uti_ocupados
        indice_isolamento_social
      }
    }
  `;

  updateBoletimEpidemiologicoMutation = gql`
    mutation updateBoletimEpidemiologico(
      $id: ID!,
      $data: DateTime,
      $quantidade_suspeitos: Int,
      $quantidade_confirmados: Int,
      $quantidade_obitos: Int,
      $quantidade_descartados: Int,
      $quantidade_curados: Int,
      $quantidade_obitos_suspeitos: Int,
      $quantidade_confirmados_previsto_fiocruz: Int
      $quantidade_testagem: Int
      $quantidade_leitos_uti: Int
      $quantidade_leitos_uti_ocupados: Int
      $indice_isolamento_social: Float
      $id_municipio: Int
      ) {
      updateBoletimEpidemiologico(
        id: $id,
        data: $data,
        quantidade_suspeitos: $quantidade_suspeitos,
        quantidade_confirmados: $quantidade_confirmados,
        quantidade_obitos: $quantidade_obitos,
        quantidade_descartados: $quantidade_descartados,
        quantidade_curados: $quantidade_curados,
        quantidade_obitos_suspeitos: $quantidade_obitos_suspeitos,
        quantidade_confirmados_previsto_fiocruz: $quantidade_confirmados_previsto_fiocruz,
        quantidade_testagem: $quantidade_testagem,
        quantidade_leitos_uti: $quantidade_leitos_uti,
        quantidade_leitos_uti_ocupados: $quantidade_leitos_uti_ocupados,
        indice_isolamento_social: $indice_isolamento_social,
        id_municipio: $id_municipio
      ) {
        id
        data
        quantidade_suspeitos
        quantidade_confirmados
        quantidade_obitos
        quantidade_descartados
        quantidade_curados
        quantidade_obitos_suspeitos
        quantidade_confirmados_previsto_fiocruz
        quantidade_testagem
        quantidade_leitos_uti
        quantidade_leitos_uti_ocupados
        indice_isolamento_social
      }
    }
  `;

  deleteBoletimEpidemiologicoMutation = gql`
    mutation deleteBoletimEpidemiologico(
      $id: ID!,
      ) {
      deleteBoletimEpidemiologico(
        id: $id,
      ) {
        id
        data
        quantidade_suspeitos
        quantidade_confirmados
        quantidade_obitos
        quantidade_descartados
        quantidade_curados
        quantidade_obitos_suspeitos
        quantidade_confirmados_previsto_fiocruz
        quantidade_testagem
        quantidade_leitos_uti
        quantidade_leitos_uti_ocupados
        indice_isolamento_social
      }
    }
  `;

  public search(params: any, idMunicipio: number): Observable<any> {
    return this.apollo.query<any>({
      query: this.getPaginatedBoletimEpidemiologicoQuery,
      variables: {
        first: params.limit,
        page: Math.floor((params.offset / params.limit) + 1),
        id_municipio: idMunicipio,
      },
      fetchPolicy: params.refetchQuery ? 'network-only' : 'cache-first',
    }).pipe(pluck('data', 'getPaginatedBoletimEpidemiologico'));
  }

  public get(id: number): Observable<any> {
    return this.apollo.query<any>({
      query: this.getBoletimEpidemiologicoQuery,
      variables: {
        id,
      },
    }).pipe(pluck('data', 'getBoletimEpidemiologico'));
  }

  public create(values: any): Observable<any> {
    return this.apollo.mutate<any>({
      mutation: this.createBoletimEpidemiologicoMutation,
      variables: values,
    }).pipe(pluck('data', 'createBoletimEpidemiologico'));
  }

  public update(idBoletim: number, values: any): Observable<any> {
    values['id'] = idBoletim;
    return this.apollo.mutate<any>({
      mutation: this.updateBoletimEpidemiologicoMutation,
      variables: values,
    }).pipe(pluck('data', 'updateBoletimEpidemiologico'));
  }

  public delete(id: number): Observable<any> {
    return this.apollo.mutate<any>({
      mutation: this.deleteBoletimEpidemiologicoMutation,
      variables: {
        id,
      },
    }).pipe(pluck('data', 'deleteBoletimEpidemiologico'));
  }
}

