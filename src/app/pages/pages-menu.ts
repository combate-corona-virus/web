import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Tutorial',
    icon: 'question-mark-circle-outline',
    link: '/pages/tutorial',
    pathMatch: 'prefix',
  },
  {
    title: 'Indicadores',
    icon: 'bar-chart-outline',
    link: '/pages/indicadores',
    home: true,
    pathMatch: 'prefix',
  },
  {
    title: 'Mapa',
    icon: 'map-outline',
    link: '/pages/mapa',
    pathMatch: 'prefix',
  },
  {
    title: 'Boletins',
    icon: 'file-text-outline',
    link: '/pages/boletins',
    pathMatch: 'prefix',
  },
  {
    title: 'Usuários',
    icon: 'people-outline',
    link: '/pages/usuarios',
    hidden: true,
    pathMatch: 'prefix',
  },
  {
    title: 'Pacientes',
    icon: 'pricetags-outline',
    link: '/pages/pacientes',
    pathMatch: 'prefix',
  },
  {
    title: 'Denúncia',
    icon: 'alert-circle-outline',
    link: '/pages/denuncias',
    hidden: true,
    pathMatch: 'prefix',
  },
  {
    title: 'Ações Sociais',
    icon: 'globe-outline',
    link: '/pages/acoes-sociais',
    hidden: true,
    pathMatch: 'prefix',
  },
  {
    title: 'FAQ',
    icon: 'question-mark-circle-outline',
    link: '/pages/faq',
    hidden: true,
    pathMatch: 'prefix',
  },
  {
    title: 'Mensagens',
    icon: 'message-circle-outline',
    link: '/pages/chat',
    pathMatch: 'prefix',
  },
  {
    title: 'Dados do município',
    icon: 'globe-outline',
    link: '/pages/municipio',
    pathMatch: 'prefix',
  },
];
