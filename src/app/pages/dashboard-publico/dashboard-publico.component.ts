import { Component, OnInit } from '@angular/core';
import { DashboardPublicoGQL } from './dashboardpublico.GQL';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'covid-dashboard',
  styleUrls: ['./dashboard-publico.component.scss'],
  templateUrl: './dashboard-publico.component.html',
})
export class DashboardPublicoComponent implements OnInit {
  idMunicipio = 3991;
  dataAtualizacao: any;

  totalizadoresAbsolutos = [];
  totalizadoresPercentuais = [];
  evolucaoCasos = [];
  obitosFaixaEtaria = [];

  symbolFormatting = (data => {
    if (data.label === '<b>Letalidade</b> <br>(óbitos pelo total)')
      return data.value + '%';
    return data.value + '';
  });

  totalizadoresAbsolutosScheme = { domain: ['#C70039', '#239B56', '#FFC300', '#5cafe2', '#8c8c8c', '#000000']};
  totalizadoresPercentuaisScheme = { domain: ['#E6B0AA', '#FAD7A0', '#ABB2B9']};
  evolucaoCasosScheme = { domain: ['#C70039', '#000000']};

  constructor(
    private dashboardPublicoGQL: DashboardPublicoGQL,
    private datePipe: DatePipe,
  ) { }

  ngOnInit() {
    this.dashboardPublicoGQL
      .getDataUltimaImportacao([this.idMunicipio])
      .subscribe(response => this.dataAtualizacao = response[0].created_at,
    );

    this.dashboardPublicoGQL
        .getGraficoTotalizadores(this.idMunicipio)
        .subscribe(data => (this.totalizadoresAbsolutos = data));

    this.dashboardPublicoGQL
      .getGraficoTotalizadoresPercentuais(this.idMunicipio)
      .subscribe(data => (this.totalizadoresPercentuais = data));

    this.dashboardPublicoGQL
      .getGraficoPatogeno(this.idMunicipio)
      .subscribe(data => {
        const newData = [];
        newData[0] = data[1];
        newData[1] = data[2];
        this.evolucaoCasos = newData;
      });

    this.dashboardPublicoGQL
      .getGraficoFaixaEtaria(this.idMunicipio)
      .subscribe(data => (this.obitosFaixaEtaria = data));
  }
}
