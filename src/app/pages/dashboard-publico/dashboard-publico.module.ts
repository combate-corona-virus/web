import { NgModule, LOCALE_ID } from '@angular/core';
import { PercentPipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbTabsetModule,
  NbUserModule,
  NbRadioModule,
  NbSelectModule,
  NbListModule,
  NbIconModule,
} from '@nebular/theme';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ThemeModule } from '../../@theme/theme.module';

import { DashboardPublicoRoutingModule, routedComponents } from './dashboard-publico-routing.module';
import { MapaCalorModule } from 'app/pages/mapa-calor/mapa-calor.module';

import localePtBr from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localePtBr);


@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbTabsetModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbListModule,
    NbIconModule,
    NbButtonModule,
    NgxChartsModule,
    DashboardPublicoRoutingModule,
    MapaCalorModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    PercentPipe,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
  ],
})
export class DashboardPublicoModule { }
