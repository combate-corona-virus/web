import { Observable } from 'rxjs/internal/Observable';
import { pluck } from 'rxjs/internal/operators/pluck';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class DashboardPublicoGQL {

  constructor(private apollo: Apollo) { }

  getGraficoTotalizadoresPercentuaisQuery = gql`
    query getGraficoTotalizadoresPercentuais($id_municipio: Int!) {
      getGraficoTotalizadoresPercentuais(id_municipio: $id_municipio) {
        name
        value
      }
    }
  `;

  getGraficoPatogenoQuery = gql`
    query getGraficoPatogeno($id_municipio: Int!) {
      getGraficoPatogeno(id_municipio: $id_municipio) {
        name
        series {
          name
          value
        }
      }
    }
  `;

  getGraficoFaixaEtariaQuery = gql`
    query getGraficoFaixaEtaria($id_municipio: Int!) {
      getGraficoFaixaEtaria(id_municipio: $id_municipio) {
        name
        value
      }
    }
  `;

  getGraficoTotalizadoresQuery = gql`
    query getGraficoTotalizadores($id_municipio: Int!) {
      getGraficoTotalizadores(id_municipio: $id_municipio) {
        name
        value
      }
    }
  `;

  getDataUltimaImportacaoQuery = gql`
    query getPaginatedPaciente($id_municipio: [Int]) {
      getPaginatedPaciente(
        first:1
        filter: {
          id_municipio: $id_municipio
          orderBy:{
            field: "created_at"
            order: DESC
          }})
        {
          data {
            created_at
          }
        }
    }
  `;

  public getGraficoTotalizadores(id_municipio: number): Observable<any> {
    return this.apollo.watchQuery<any>({
      query: this.getGraficoTotalizadoresQuery,
      variables: {
        id_municipio,
      },
    }).valueChanges.pipe(pluck('data', 'getGraficoTotalizadores'));
  }

  public getGraficoPatogeno(id_municipio: number): Observable<any> {
    return this.apollo.watchQuery<any>({
      query: this.getGraficoPatogenoQuery,
      variables: {
        id_municipio,
      },
    }).valueChanges.pipe(pluck('data', 'getGraficoPatogeno'));
  }

  public getGraficoFaixaEtaria(id_municipio: number): Observable<any> {
    return this.apollo.watchQuery<any>({
      query: this.getGraficoFaixaEtariaQuery,
      variables: {
        id_municipio,
      },
    }).valueChanges.pipe(pluck('data', 'getGraficoFaixaEtaria'));
  }

  public getGraficoTotalizadoresPercentuais(id_municipio: number): Observable<any> {
    return this.apollo.watchQuery<any>({
      query: this.getGraficoTotalizadoresPercentuaisQuery,
      variables: {
        id_municipio,
      },
    }).valueChanges.pipe(pluck('data', 'getGraficoTotalizadoresPercentuais'));
  }

  public getDataUltimaImportacao(id_municipio: number[]): Observable<any> {
    return this.apollo.query<any>({
      query: this.getDataUltimaImportacaoQuery,
      variables: {
        id_municipio,
      },
    }).pipe(pluck('data', 'getPaginatedPaciente', 'data'));
  }

}

