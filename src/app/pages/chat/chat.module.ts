import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbTreeGridModule,
  NbButtonModule,
  NbActionsModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbUserModule,
  NbSelectModule,
  NbRadioModule,
} from '@nebular/theme';


import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { FileUploadModule } from 'primeng/fileupload';

import { ChatResolver } from 'app/providers/services/chat-resolver.service';
import { ChatRoutingModule, routedComponents } from './chat-routing.module';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ChatRoutingModule,
    TableModule,
    ButtonModule,
    NbButtonModule,
    FormsModule,
    ReactiveFormsModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule,
    FileUploadModule,
    CommonModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    ChatResolver,
  ],
})
export class ChatModule { }
