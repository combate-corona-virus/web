import { Observable } from 'rxjs/internal/Observable';
import { pluck } from 'rxjs/internal/operators/pluck';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root',
})
export class ChatGQL {
  constructor(private apollo: Apollo) {}

  getPaginatedGroupedChatQuery = gql`
    query getPaginatedGroupedChat($first: Int!, $page: Int, $filter: GroupedChatFilterInput) {
      getPaginatedGroupedChat(first: $first, page: $page, filter: $filter) {
        paginatorInfo {
          total
        }
        data {
          id
          created_at
          id_dispositivo
          nome_dispositivo
          texto
          enviado_por
          id_municipio
          municipio{
            nome
          }
        }
      }
    }
  `;

  getAllChatQuery = gql`
    query getAllChat($filter: ChatFilterInput) {
      getAllChat(filter: $filter) {
        id
        id_municipio
        created_at
        enviado_por
        id_dispositivo
        nome_dispositivo
        texto
      }
    }
  `;

  createChatMutation = gql`
    mutation createChat($input: CreateChatInput!) {
      createChat(input: $input) {
        id
        id_municipio
        created_at
        enviado_por
        id_dispositivo
        nome_dispositivo
        texto
      }
    }
  `;

  public getPaginatedGroupedChat(params: any, idMunicipio: number): Observable<any> {
    return this.apollo.query<any>({
      query: this.getPaginatedGroupedChatQuery,
      variables: {
        first: params.limit,
        page: Math.floor((params.offset / params.limit) + 1),
        filter: {
          id_municipio: idMunicipio,
          enviado_por: 'MUNICIPE',
          orderBy: [{
            field: 'created_at',
            order: 'DESC',
          }],
        },
      },
    }).pipe(pluck('data', 'getPaginatedGroupedChat'));
  }

  public getAllMessages(idMunicipio: number, idDispositivo: string): Observable<any> {
    return this.apollo.query<any>({
      query: this.getAllChatQuery,
      variables: {
        filter: {
          id_municipio: idMunicipio,
          id_dispositivo: idDispositivo,
          orderBy: [{
            field: 'created_at',
            order: 'ASC',
          }],
        },
      },
      fetchPolicy: 'network-only',
    }).pipe(pluck('data', 'getAllChat'));
  }

  public createMessage(values: any, message: any): Observable<any> {
    return this.apollo.mutate<any>({
      mutation: this.createChatMutation,
      variables: {
        input: {
          enviado_por: 'PREFEITURA',
          id_dispositivo: values.id_dispositivo,
          nome_dispositivo: 'Prefeitura',
          id_municipio: values.id_municipio,
          texto: message,
        },
      },
    }).pipe(pluck('data', 'createChat'));
  }
}
