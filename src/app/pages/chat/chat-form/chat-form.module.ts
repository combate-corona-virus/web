import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbTreeGridModule,
  NbButtonModule,
  NbChatModule,
} from '@nebular/theme';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';

import { ChatFormRoutingModule, routedComponents } from './chat-form-routing.module';

@NgModule({
  imports: [
    CommonModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ChatFormRoutingModule,
    TableModule,
    ButtonModule,
    NbButtonModule,
    NbChatModule,
  ],
  declarations: [...routedComponents],
})
export class ChatFormModule {}
