import { Component, OnInit } from '@angular/core';
import { ChatGQL } from '../chat.GQL';
import { ActivatedRoute } from '@angular/router';
import { RouterHelper } from 'app/providers/utils/router-helper.util';

@Component({
  selector: 'covid-chat-form',
  styleUrls: ['./chat-form.component.scss'],
  templateUrl: './chat-form.component.html',
})
export class ChatFormComponent implements OnInit {

  resolvedChat: any;

  constructor(
    private route: ActivatedRoute,
    private chatGQL: ChatGQL,
  ) {}

  ngOnInit() {
    RouterHelper.getResolvedData(this.route, 'chat').subscribe(resolvedData => {
      this.resolvedChat = resolvedData;
    });
  }

  sendMessage(event) {
    this.chatGQL.createMessage(this.resolvedChat[0], event.message).subscribe(() => {
      // tslint:disable-next-line: max-line-length
      this.chatGQL.getAllMessages(this.resolvedChat[0].id_municipio, this.resolvedChat[0].id_dispositivo).subscribe(messages => {
        this.resolvedChat = messages;
      });
    });
  }
}
