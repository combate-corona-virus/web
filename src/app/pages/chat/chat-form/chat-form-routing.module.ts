import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChatFormComponent } from './chat-form.component';

const routes: Routes = [
  {
  path: '',
  component: ChatFormComponent,
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChatFormRoutingModule { }

export const routedComponents = [
    ChatFormComponent,
];
