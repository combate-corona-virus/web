import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { TabelaBase } from 'app/classes/TabelaBase';
import { DataTableConfig } from 'app/classes/DataTableConfig';
import { ChatGQL } from '../chat.GQL';
import { AuthService } from 'app/providers/services/auth.service';

@Component({
  selector: 'covid-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.scss'],
})
export class ChatListComponent extends TabelaBase implements OnInit {

  chat: any;
  dataTableConfig = new DataTableConfig({ emptyMessage: 'Nenhum Registro Encontrado' });
  idMunicipio: number = AuthService.getToken().id_municipio;

  constructor(
    router: Router,
    route: ActivatedRoute,
    private fb: FormBuilder,
    private chatGLQ: ChatGQL,
  ) {
    super(router, route);
  }

  ngOnInit() {
    this.form = this.fb.group({});
    this.listObservableCallback = params => this.chatGLQ.getPaginatedGroupedChat({ ...params }, this.idMunicipio);
    super.initializeComponent();
  }
}
