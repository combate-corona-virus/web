import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChatListComponent } from './chat-list.component';

const routes: Routes = [
  {
    path: '',
    component: ChatListComponent,
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChatListRoutingModule { }

export const routedComponents = [
    ChatListComponent,
];
