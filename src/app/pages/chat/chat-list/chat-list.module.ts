import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule } from '@nebular/theme';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { FileUploadModule } from 'primeng/fileupload';

import { ChatListRoutingModule, routedComponents } from './chat-list-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SomaCalendarModule } from 'app/components/soma-calendar/soma-calendar.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ChatListRoutingModule,
    TableModule,
    ButtonModule,
    NbButtonModule,
    FileUploadModule,
    SomaCalendarModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class ChatListModule { }
