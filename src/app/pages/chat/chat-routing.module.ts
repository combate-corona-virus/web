import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChatResolver } from 'app/providers/services/chat-resolver.service';
import { ChatComponent } from './chat.component';


const routes: Routes = [
  {
    path: '',
    component: ChatComponent,
    children: [
      {
        path: 'list',
        loadChildren: () => import('./chat-list/chat-list.module')
          .then(m => m.ChatListModule),
      },
      {
        path: ':id_municipio/:id_dispositivo',
        resolve: { chat: ChatResolver },
        children: [
          {
            path: 'messages',
            loadChildren: () => import('./chat-form/chat-form.module')
              .then(m => m.ChatFormModule),
          },
          {
            path: '**',
            redirectTo: 'messages',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: '**',
        redirectTo: 'list',
        pathMatch: 'full',
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChatRoutingModule { }

export const routedComponents = [
  ChatComponent,
];
