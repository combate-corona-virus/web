import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { PagesGuard } from '../providers/services/pages-guard.service';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  canActivate: [PagesGuard],
  children: [
    {
      path: 'indicadores',
      loadChildren: () => import('./dashboard/dashboard.module')
        .then(m => m.DashboardModule),
    },
    {
      path: 'mapa',
      loadChildren: () => import('./maps/maps.module')
        .then(m => m.MapsModule),
    },
    {
      path: 'boletins',
      loadChildren: () => import('./boletins/boletins.module')
        .then(m => m.BoletinsModule),
    },
    {
      path: 'usuarios',
      loadChildren: () => import('./usuarios/usuarios.module')
        .then(m => m.UsuariosModule),
    },
    {
      path: 'perfil',
      loadChildren: () => import('./perfil/perfil.module')
        .then(m => m.PerfilModule),
    },
    {
      path: 'municipio',
      loadChildren: () => import('./municipio/municipio.module')
        .then(m => m.MunicipioModule),
    },
    {
      path: 'pacientes',
      loadChildren: () => import('./pacientes/pacientes.module')
      .then(m => m.PacientesModule),
    },
    {
      path: 'denuncias',
      loadChildren: () => import('./denuncias/denuncias.module')
        .then(m => m.DenunciasModule),
    },
    {
      path: 'acoes-sociais',
      loadChildren: () => import('./acoes-sociais/acoes-sociais.module')
        .then(m => m.AcoesSociaisModule),
    },
    {
      path: 'faq',
      loadChildren: () => import('./faq/faq.module')
        .then(m => m.FaqModule),
    },
    {
      path: 'tutorial',
      loadChildren: () => import('./tutorial/tutorial.module')
        .then(m => m.TutorialModule),
    },
    {
      path: 'chat',
      loadChildren: () => import('./chat/chat.module')
        .then(m => m.ChatModule),
    },
    {
      path: '**',
      redirectTo: 'indicadores',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
