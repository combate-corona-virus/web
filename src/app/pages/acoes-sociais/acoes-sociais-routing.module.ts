import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcoesSociaisComponent } from './acoes-sociais.component';

const routes: Routes = [
  {
    path: '',
    component: AcoesSociaisComponent,
    children: [
      {
        path: 'list',
        loadChildren: () => import('./acoes-sociais-list/acoes-sociais-list.module')
        .then(m => m.AcoesSociaisListModule),
      },
      {
        path: 'add',
        loadChildren: () => import('./acoes-sociais-form/acoes-sociais-form.module')
        .then(m => m.AcoesSociaisFormModule),
      },
      {
        path: ':id',
        children: [
          {
            path: 'edit',
            loadChildren: () => import('./acoes-sociais-form/acoes-sociais-form.module')
            .then(m => m.AcoesSociaisFormModule),
          },
          {
            path: '**',
            redirectTo: 'edit',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: '**',
        redirectTo: 'list',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AcoesSociaisRoutingModule { }

export const routedComponents = [
  AcoesSociaisComponent,
];
