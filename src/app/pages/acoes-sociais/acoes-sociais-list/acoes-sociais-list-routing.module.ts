import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AcoesSociaisListComponent } from './acoes-sociais-list.component';

const routes: Routes = [
  {
  path: '',
  component: AcoesSociaisListComponent,
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AcoesSociaisListRoutingModule { }

export const routedComponents = [
    AcoesSociaisListComponent,
];
