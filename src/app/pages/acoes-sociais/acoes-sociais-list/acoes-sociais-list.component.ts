import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'covid-acoes-sociais-list',
  templateUrl: './acoes-sociais-list.component.html',
  styleUrls: ['./acoes-sociais-list.component.scss'],
})
export class AcoesSociaisListComponent implements OnInit {

  acoesSociais: any;

  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.acoesSociais = [
      {
        'id': 1,
        'titulo': 'teste 1',
        'municipio': 'Municipio 1',
        'data_inicio': '27/03/2020',
        'data_encerramentto': '27/03/2020',
      },
    ];
  }

  goToCreateAcaoSocial() {
    this.router.navigate(['pages/acoes-sociais/add']);
  }

  goToEditAcaoSocial(id: number) {
    this.router.navigate([`pages/acoes-sociais/${id}/edit`]);
  }

}
