import { AcoesSociaisListRoutingModule, routedComponents } from './acoes-sociais-list-routing.module';
import { NgModule } from '@angular/core';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule } from '@nebular/theme';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    AcoesSociaisListRoutingModule,
    TableModule,
    ButtonModule,
    NbButtonModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class AcoesSociaisListModule { }
