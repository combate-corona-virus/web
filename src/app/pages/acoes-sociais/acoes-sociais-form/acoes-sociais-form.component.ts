import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'covid-form-acoes-sociais',
  templateUrl: './acoes-sociais-form.component.html',
  styleUrls: [ './acoes-sociais-form.component.scss'],
})
export class AcoesSociaisFormComponent  implements OnInit {

    form: FormGroup;

    constructor(private formBuilder: FormBuilder, private location: Location) { }

    ngOnInit() {
        this.form = this.formBuilder.group({
            'titulo': [null, Validators.required],
            'descricao': [null, Validators.required],
            'municipio': [null, Validators.required],
            'data_inicio': [null, Validators.required],
            'data_encerramento': [null, Validators.required],
        }, {});
    }

    submit(formValue: any) {}

    cancel() {
        this.location.back();
      }
}
