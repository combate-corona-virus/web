import { NgModule } from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from '@nebular/theme';
import { FormsModule as ngFormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThemeModule } from '../../../@theme/theme.module';
import { CommonModule } from '@angular/common';
import { AcoesSociaisFormRoutingModule } from './acoes-sociais-form-routing.module';
import {FileUploadModule} from 'primeng/fileupload';
import { AcoesSociaisFormComponent } from './acoes-sociais-form.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AcoesSociaisFormRoutingModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    FileUploadModule,
  ],
  declarations: [
    AcoesSociaisFormComponent,
  ],
})
export class AcoesSociaisFormModule { }
