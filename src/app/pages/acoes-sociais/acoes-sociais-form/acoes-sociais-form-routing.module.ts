import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AcoesSociaisFormComponent } from './acoes-sociais-form.component';

const routes: Routes = [
  {
  path: '',
  component: AcoesSociaisFormComponent,
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AcoesSociaisFormRoutingModule { }

export const routedComponents = [
  AcoesSociaisFormComponent,
];
