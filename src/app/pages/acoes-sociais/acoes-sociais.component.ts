import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'covid-acoes-sociais',
  template: `<router-outlet></router-outlet>`,
})
export class AcoesSociaisComponent { }
