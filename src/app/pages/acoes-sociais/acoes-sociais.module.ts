import { NgModule } from '@angular/core';

import { AcoesSociaisRoutingModule, routedComponents } from './acoes-sociais-routing.module';

@NgModule({
  imports: [
    AcoesSociaisRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class AcoesSociaisModule { }
