import { HttpService } from './http.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable()
export class ImportacaoService {

  /**
   * URL do endpoint.
   * @type {String}
   */
  private endpoint = '/paciente/importacao';

  /**
   * [constructor description]
   * @param {HttpInterceptor} private http [description]
   */
  constructor(private http: HttpService ) {}

  downloadPlanilha(): Observable<any> {
    return this.http.getFile(`${this.endpoint}`, `Combate Coronavírus - Planilha de controle de pacientes.xlsx`);
  }

  postFile(file: any) {
    const adEndpoint = '/importacoes-validacao';
    const values = {
      file,
    };
    return this.http.postFile(adEndpoint, values);
  }

  postWithFile(
    file: File,
  ): Observable<any> {
    return this.http.postWithFile(`${this.endpoint}`, {}, file);
  }

  uploadLogo(file: File): Observable<any> {
    return this.http.postWithFile('/municipio/logo', {}, file);
  }
}
