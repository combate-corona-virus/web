import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { PacienteGQL } from 'app/pages/pacientes/paciente.GQL';
import { ResolverHelper } from 'app/providers/utils/resolver-helper.util';

@Injectable()
export class PacienteResolver extends ResolverHelper implements Resolve<any> {

  constructor(public pacienteGQL: PacienteGQL, router: Router) {
    super(router, '/pages/pacientes');
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {

    const idPaciente = +route.params['id'];
    const observable = this.pacienteGQL.get(idPaciente);

    return this.checkResource(observable);
  }

}
