import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { BoletimGQL } from 'app/pages/boletins/boletim.GQL';
import { ResolverHelper } from 'app/providers/utils/resolver-helper.util';

@Injectable()
export class BoletimResolver extends ResolverHelper implements Resolve<any> {

  constructor(public boletimGQL: BoletimGQL, router: Router) {
    super(router, '/pages/boletins');
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {

    const idBoletim = +route.params['id'];
    const observable = this.boletimGQL.get(idBoletim);

    return this.checkResource(observable);
  }

}
