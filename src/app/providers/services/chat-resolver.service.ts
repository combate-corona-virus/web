import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { ChatGQL } from 'app/pages/chat/chat.GQL';
import { ResolverHelper } from 'app/providers/utils/resolver-helper.util';

@Injectable()
export class ChatResolver extends ResolverHelper implements Resolve<any> {

  constructor(public chatGQL: ChatGQL, router: Router) {
    super(router, '/pages/chat');
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const idMunicipio = +route.params['id_municipio'];
    const idDispositivo = route.params['id_dispositivo'];
    const observable = this.chatGQL.getAllMessages(idMunicipio, idDispositivo);

    return this.checkResource(observable);
  }

}

