import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/internal/operators/filter';
import { AuthService } from './providers/services/auth.service';
@Component({
  selector: 'covid-app',
  template: `<router-outlet></router-outlet>`,
})
export class AppComponent implements OnInit {

  constructor(
    private router: Router,
  ) {}

  ngOnInit() {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd && event.urlAfterRedirects === '/pages'),
    ).subscribe(() => {
      AuthService.getToken() ? this.router.navigate(['/pages']) : this.router.navigate(['/login']);
    });
  }
}
