import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { LoginGuard } from './providers/services/login-guard.service';
import { AuthenticatedUserGuard } from './providers/services/authenticated-user-guard.service';

import { HomepageComponent } from './homepage/homepage.component';

export const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: HomepageComponent,
      },
      // {
      //   path: '',
      //   loadChildren: () => import('./pages/dashboard-publico/dashboard-publico.module')
      //     .then(m => m.DashboardPublicoModule),
      // },
      {
        path: 'mapa-calor/:id',
        loadChildren: () => import('./pages/mapa-calor/mapa-calor.module')
          .then(m => m.MapaCalorModule),
      },
      {
        path: 'pages',
        canLoad: [AuthenticatedUserGuard],
        loadChildren: () => import('./pages/pages.module')
          .then(m => m.PagesModule),
      },
      {
        path: 'login',
        canActivate: [LoginGuard],
        loadChildren: () => import('./pages/login/login.module')
          .then(m => m.LoginModule),
      },
    ],
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
  useHash: true, scrollPositionRestoration: 'enabled',
});
