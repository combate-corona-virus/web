export class DataTableConfig {
  defaultFirstRow = 1;
  defaultRowsPerPage = 30;

  firstRow = 1;
  rowsPerPage = 30;

  sortField: string;
  sortOrder: number;

  maxPageLinks = 5;
  rowsPerPageOptions: number[] = [10, 20, 30];
  emptyMessage = 'Nenhum Dado Encontrado';

  constructor(options?: { emptyMessage: string }) {
    if (options) {
      this.emptyMessage = options.emptyMessage;
    }
  }
}
