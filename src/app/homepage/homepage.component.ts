import { Component } from '@angular/core';

@Component({
  selector: 'covid-homepage',
  styleUrls: ['./homepage.component.scss'],
  templateUrl: './homepage.component.html',
})

export class HomepageComponent {

  constructor() {}

  animatedScroll(elementId: string) {
    document.getElementById(elementId).scrollIntoView({ behavior: 'smooth' });
  }

}
