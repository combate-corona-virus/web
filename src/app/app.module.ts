
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { routing } from './app-routing.module';
import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule,
} from '@nebular/theme';
import { GraphQLModule } from './graphql/graphql.module';
import { AuthService } from './providers/services/auth.service';
import { GlobalState } from './global.state';
import { httpInterceptorProviders } from './providers/interceptors/http-interceptor-providers';
import { MapService } from './providers/services/map.service';
import { LoggedUserService } from './providers/services/logged-user.service';
import { PacienteGQL } from './pages/pacientes/paciente.GQL';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    routing,

    ThemeModule.forRoot(),

    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbChatModule.forRoot({
      messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    }),
    CoreModule.forRoot(),
    GraphQLModule,
  ],
  providers: [
    AuthService,
    GlobalState,
    httpInterceptorProviders,
    MapService,
    LoggedUserService,
    PacienteGQL,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
